import React, { useEffect } from "react";
import Login from "./components/auth/login/login";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import NavBar from "./components/layout/navbar/main_navbar/main_navbar";
import TopNav from "./components/layout/navbar/topnav/topnav";
import Footer from "./components/layout/footer/footer";
import Home from "./components/home/home";
import SearchTutor from "./components/search_tutors/search_tutor";

import AboutUS from "./components/about_us/about_us";
import ContactUS from "./components/contact_us/contact_us";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import Signup from "./components/auth/signup/signup";
import ForgotPassword from "./components/auth/forgot-password";
import UpdatePassword from "./components/auth/update-password";
import Profile from "./components/profile/profile";
import Setting from "./components/setting/setting";
import { login, save_user_role } from "./actions/authentication";
import axios from "axios";
import FAQHome from "./components/faq/faq_home";
import TechnicalSupport from "./components/faq/technical_support";
import HelpForParentAndStudents from "./components/faq/help_for_parents_and_parents";
import HelpForTutors from "./components/faq/help_for_tutors";
import constants from "./config";
import { async } from "q";
import ForTutor from "./components/ui_components/for Tutor/ForTutor";

function App() {
  const dispatch = useDispatch();

  axios.defaults.headers.common["auth_token"] = localStorage.getItem(
    "auth_token"
  );
  axios.defaults.headers.common["Content-Type"] = localStorage.getItem(
    "multipart/form-data"
  );
  useEffect(() => {
    if (
      localStorage.getItem("auth_token") !== "undefined" &&
      typeof localStorage.getItem("auth_token") !== undefined &&
      localStorage.getItem("auth_token")
    ) {
      axios.get(constants.Base_URL + "v1/user_role").then(
        async(res => {
          dispatch(save_user_role(res.data.body.user_role));
        })
      );
      dispatch(login());
    } else {
    }
    return () => {};
  }, []);

  const auth_status = useSelector(state => state.authentication_status);

  return (
    <div className={useSelector(state => state.background_clr)}>
      <div className="row landing">
        <BrowserRouter>
          {/* {useSelector(state => state.Navbar == "none" ? null : <TopNav  />)} */}

          {useSelector(state => (state.Navbar == "none" ? null : <NavBar />))}

          <Switch>
            <Route path="/" exact render={props => <Home {...props} />} />
            <Route path="/about-us" render={props => <AboutUS {...props} />} />

            <Route
              path="/contact-us"
              render={props => <ContactUS {...props} />}
            />
            <Route
              path="/faq/technical_support"
              render={props => <TechnicalSupport {...props} />}
            />
            <Route
              path="/faq/Help_for_tutors"
              render={props => <HelpForTutors {...props} />}
            />
            <Route
              path="/faq/Help_for_Parents&Students"
              render={props => <HelpForParentAndStudents {...props} />}
            />
            <Route path="/faq" render={props => <FAQHome {...props} />} />

            <Route
              path="/searchtutor"
              render={props => <SearchTutor {...props} />}
            />
            <Route path="/login" render={props => <Login {...props} />} />
            <Route path="/forgot-password" component={ForgotPassword} />
            <Route path="/update_password/:token" component={UpdatePassword} />
            <Route path="/signup" render={props => <Signup {...props} />} />
            <Route
              path="/tutor_profile/:id"
              render={props => <Profile {...props} />}
            />
            <Route path="/profile" render={props => <Profile {...props} />} />
            <Route path="/setting" render={props => <Setting {...props} />} />
            <Route path="/forTutor" component={ForTutor} />
          </Switch>

          <Footer />
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
