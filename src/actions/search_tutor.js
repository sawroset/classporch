
export const HOURLY_RATE = (payload) => {
    return {
        type: 'rates',
        payload
    }
}

export const LEVELS = (payload) => {
    return {
        type: 'levels',
        payload
    }
}

export const STATUS = (payload) => {
    return {
        type: 'toggle',
        payload
    }
}
export const RATING = (payload) => {
    return {
        type: 'rating',
        payload
    }
}
export const LOCATION = (payload) => {
    return {
        type: 'location',
        payload
    }
}

export const SHCOOL_NAME = (payload) => {
    return {
        type: 'school_name',
        payload
    }
}

export const LANGUAGE = (payload) => {
    return {
        type: 'language',
        payload
    }
}

export const SEARCHBAR_LOCATION = (payload) => {
    return {
        type: 'searchbar_location',
        payload
    }
}


export const SEARCHBAR_NAME = (payload) => {
    return {
        type: 'searchbar_name',
        payload
    }
}

export const NAVPILLS = (payload) => {
    return {
        type: 'nav_pills_items',
        payload 
    }
}