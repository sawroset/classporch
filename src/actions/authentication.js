export const login = () => {
  return {
    type: "login"
  };
};

export const Log_out = () => {
  return {
    type: "logout"
  };
};

export const save_auth_user_info = user => {
  console.log(user);
  return {
    type: "save_auth_user_info",
    payload: user
  };
};

export const remove_auth_user_info = () => {
  return {
    type: "remove_auth_user_info"
  };
};

export const save_user_role = role => {
  return {
    type: "save_user_role",
    payload: role
  };
};
