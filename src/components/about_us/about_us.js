import React, { useEffect } from "react";
import "./about_us.css";

import OurStory from "../ui_components/our_story/our_story";
import Who_We_Are from "../ui_components/who_we_are/who_we_are";
import { useDispatch } from "react-redux";
import {
  Navbar_clr_white,
  Navbar_clr_grey,
  Background_clr_grey
} from "../../actions/theme";
const AboutUS = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(Navbar_clr_white());
    window.scrollTo(0, 0);
    return () => {
      dispatch(Navbar_clr_white());
      dispatch(Background_clr_grey());
    };
  }, []);
  return (
    <div className="">
      <OurStory
        title="Class Porch online Totors"
        discription="It is a long established fact that a reade"
        slogan="Cras tristique turpis justo, eu consequat sem adipiscing ut. Donec posuere bibendum metus."
        aut="Tony Nguyen, Chancellor"
      />

      <Who_We_Are
        img_src="/assets/bambini.png"
        title="WHO WE ARE"
        discription="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quis eros lobortis, vestibulum turpis ac, pulvinar odio. Praesent vulputate a elit ac mollis. In sit amet ipsum turpis. Pellentesque venenatis, libero vel euismod lobortis, mi metus luctus augue, eget dapibus elit nisi eu massa. Phasellus sollicitudin nisl posuere nibh ultricies, et fringilla dui gravida. "
        discription_sub="Donec iaculis adipiscing neque, non congue massa euismod quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      />

      <Who_We_Are
        class_name="invert"
        img_src="/assets/laureata.png"
        title="WHAT WE DO"
        discription="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quis eros lobortis, vestibulum turpis ac, pulvinar odio. Praesent vulputate a elit ac mollis. In sit amet ipsum turpis. Pellentesque venenatis, libero vel euismod lobortis, mi metus luctus augue, eget dapibus elit nisi eu massa. Phasellus sollicitudin nisl posuere nibh ultricies, et fringilla dui gravida. "
        discription_sub=""
      />
    </div>
  );
};

export default AboutUS;
