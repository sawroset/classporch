import React,{useEffect} from "react";
import Banner  from '../ui_components/banner/banner';
import './faq_child.css';
import './faq.css';
import Suggested_Faq from '../ui_components/suggested_faq/suggested_faq';
import {
  Navbar_clr_white,
  Navbar_clr_grey,
  Background_clr_white,
  Background_clr_grey
} from "../../actions/theme";
import {useDispatch} from 'react-redux'
const HelpForParentAndStudents = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    window.scrollTo(0,0);
    dispatch(Background_clr_white());
    dispatch(Navbar_clr_white());

    return ()=>{
            
    dispatch(Background_clr_grey());
    dispatch(Navbar_clr_grey());
    }
  },[]);
    const questions= [
        {
          question:'How do I pay for Online Lessons?',
          answer:'sample text hello hello testing'
        },
        {
          question:'What will happen in the interview?',
          answer:'sample text hello hello testing'
        },
        {
          question:`How do I refund a lesson if it doesn't go ahead?`,
          answer:'sample text hello hello testing'
        },
        {
          question:'Temporarily clear your settings',
          answer:'sample text hello hello testing'
        },
        {
          question:'How do I watch back Online Lesson recordings after they are completed?',
          answer:'sample text hello hello testing'
        },
        {
          question:'How to Refer Friends to Become Tutors?',
          answer:'sample text hello hello testing'
        },
     
      ];
        return (
            <div>
          
              <div className="banner_2">
                   <h1 className="faq_title">Help to Parents and Students</h1>
                   </div>
            
              <Suggested_Faq title="parents and students faq" questions={questions}/>
            </div>
        );
};

export default HelpForParentAndStudents;
