import React, { useEffect } from "react";
import "./faq.css";
import Banner from "../ui_components/banner/banner";
import Faq_Help from "../ui_components/faq_help/faq_help";
import Suggested_Faq from "../ui_components/suggested_faq/suggested_faq";
import {
  Navbar_clr_white,
  Navbar_clr_grey,
  Background_clr_white,
  Background_clr_grey
} from "../../actions/theme";
import { useDispatch } from "react-redux";
const FAQHome = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(Background_clr_white());
    dispatch(Navbar_clr_white());

    return () => {
      dispatch(Background_clr_grey());
      dispatch(Navbar_clr_grey());
    };
  }, []);
  const questions = [
    {
      question: "How do I pay for Online Lessons?",
      answer: "sample text hello hello testing"
    },
    {
      question: "What will happen in the interview?",
      answer: "sample text hello hello testing"
    },
    {
      question: `How do I refund a lesson if it doesn't go ahead?`,
      answer: "sample text hello hello testing"
    },
    {
      question: "Temporarily clear your settings",
      answer: "sample text hello hello testing"
    },
    {
      question:
        "How do I watch back Online Lesson recordings after they are completed?",
      answer: "sample text hello hello testing"
    },
    {
      question: "How to Refer Friends to Become Tutors?",
      answer: "sample text hello hello testing"
    }
  ];
  const handleRoute = value => {
    props.history.push(value);
  };
  return (
    <div>
      <div className="banner_2">
        <h1 className="faq_title">FAQ Page</h1>
      </div>

      <div className="help_parent">
        <div className="col-sm-1"></div>
        <Faq_Help
          title="Help for Parents & Students"
          img_path="/assets/persons.png"
          handleRoute={handleRoute}
          url={`${props.match.url}/Help_for_Parents&Students`}
        />

        <Faq_Help
          title="Help for tutors"
          img_path="/assets/books.png"
          handleRoute={handleRoute}
          url={`${props.match.url}/Help_for_tutors`}
        />

        <Faq_Help
          title="technical support"
          img_path="/assets/pc.png"
          handleRoute={handleRoute}
          url={`${props.match.url}/technical_support`}
        />
        <div className="col-sm-1"></div>
      </div>

      <Suggested_Faq title="suggested fAQ" questions={questions} />
    </div>
  );
};

export default FAQHome;
