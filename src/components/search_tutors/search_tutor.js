import React, { useState,useEffect } from "react";
import "./search_tutor.css";
import Banner from "../ui_components/banner/banner";
import SearchBar from "../ui_components/search_bar/search_bar";
import Filters from "../ui_components/filters/filters";

import Tutors from "./tutors";
import {
  Navbar_clr_white,
  Navbar_clr_grey,
  Background_clr_white,
  Background_clr_grey
} from "../../actions/theme";
import {  useDispatch } from "react-redux";
import axios from "axios";
import constants from "../../config";
const SearchTutor = props => {
  const [tutorName, setTutorName] = useState("");
  const [subjectName, setSubjectName] = useState("");
  const [location, setLocation] = useState("");
  const [minRate, setMinRate] = useState("25");
  const [maxRate, setMaxRate] = useState("100");
  const [gender, setGender] = useState("");
  const [language, setLanguage] = useState("");
  const [status, setStatus] = useState(false);
  const [rating, setRatting] = useState([]);
  const [tutors,setTutors] = useState([]);
  const [page_number,setPage_number] = useState('1');
  const [total_records,setTotal_Records] = useState([]);
  
  


  const dispatch = useDispatch();

  useEffect(() => {
    window.scrollTo(0,0);
    dispatch(Background_clr_white());
    dispatch(Navbar_clr_white());
    axios.get(
      constants.Base_URL +
        "v1/search?role=tutor&type=" +
        subjectName +
        "&q=" +
        tutorName +
        "&gender=" +
        gender +
        "&min_rate=" +
        minRate +
        "&max_rate=" +
        maxRate +
        "&page_no="+page_number
    ).then((res)=>{
     let tr=res.data.response.total_count;
    
     
     setTotal_Records(tr);
     
     
      setTutors(res.data.response.users);
    });
  
    return ()=>{
      
    dispatch(Background_clr_grey());
    dispatch(Navbar_clr_grey());
    }
  }, [])
  const handleTutorName = e => {
    setTutorName(e.target.value);
  }; 
 
  const handleSubjectName = e => {
    setSubjectName(e.target.value);
  };
  const handleLocation = e => {
    setLocation(e.target.value);
  };
  const handleGender = e => {
    setGender(e.target.value);
  };
  const hanldeLanguage = e => {
    setLocation(e.target.value);
  };

  const handleMinRate = (value,update_view) => {
    console.log(value)
    setMinRate(value);
    update_view();

  };

  const handleMaxRate = (value,update_view) => {
    console.log(value)
    setMaxRate(value);
    update_view();

  };

  
  const handleSearch = () => {

    console.log("searching again")
    axios.get(
      constants.Base_URL +
        "v1/search?role=tutor&type=" +
        subjectName +
        "&q=" +
        tutorName +
        "&gender=" +
        gender +
        "&min_rate=" +
        minRate +
        "&max_rate=" +
        maxRate +
        "&page_no="+page_number
    ).then(async(res)=>{
      let tr=res.data.response.total_count;
     
     await setTotal_Records(tr);   
     
      console.log(res.data.response.users.length);
      await setTutors(res.data.response.users);
      
    });
  };

  const handlePagination = async  (value,update_view) =>{ 
   await setPage_number(value);
   await handleSearch();
   update_view();
  }

  const handleProfile = value => { 
    props.history.push('/tutor_profile/'+value);
  };


  return (
    <div >
      

      <SearchBar
        handleSubjectName={handleSubjectName}
        handleTutorName={handleTutorName}
        handleSearch={handleSearch}
      />
      <br />
      <br />

      <div className="row">
        <Filters
          handleGender={handleGender}
          handleLocation={handleLocation} 
          hanldeLanguage={hanldeLanguage}
          handleMinRate={handleMinRate}
          handleMaxRate={handleMaxRate}
          minRate={minRate}
          maxRate={maxRate}
        />

        <br />
        <br />
        <Tutors handleProfile={handleProfile} handlePagination={handlePagination} total_records={total_records} tutor_collection={tutors}/>
        <br/>
        <br/>
  
      </div>
    </div>
  );
};

export default SearchTutor;
