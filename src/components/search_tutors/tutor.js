import React from 'react';
import './tutor.css';
const Tutor = props => {



    const tags = props.tags.map((item)=>{
        return(
            <span className="scuola2">{item.name}</span> 
        )
    });
    

return (
        <div className="row mtop40 search_tutor">
            <div className="col-sm-2">
                <img src={props.profile_pic} className="img-responsive img-circle center-block pimg" alt="" /> <br />
                <p className="text-center"><i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <br />
                    5.0 ({props.rating})</p>
            </div>    
      
            <div className="col-sm-7">
                <strong onClick={()=>{props.handleProfile(props.profile_id)}}>{props.name}</strong><br />
                <p className="titplus">{props.intro}</p>
                <p>{props.discription}</p>
                <p className="mbottom10"><i className="fa fa-map-marker" aria-hidden="true"></i>{props.location}</p>
            
                <p>{tags}</p>
            </div>

            <div className="col-sm-3 text-right">
                <div>
                    <h3 className="mbottom0">USD${props.price}</h3>
                    per 15 min <br /><br />
                    <div className="btn_group">
                        <span className="xm center-block">MESSAGE</span>
                        <span className="xb center-block">REQUEST SESSION</span>
                    </div>
                </div>
            </div> 

        </div>

    );
}

export default Tutor;
