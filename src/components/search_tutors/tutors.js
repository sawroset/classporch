import React ,{useState, useEffect} from 'react';
import Tutor from './tutor';
import Pagination from "react-js-pagination";
import './tutors.css';
const Tutors = props => {

    const [activePage,setActivePage] = useState('1');
    // const [pages,setPages]=useState([false]);
    const [, forceUpdate] = useState();
    
    
    //  useEffect(()=>{
    //         setPages(props.total_pages);
    //  },[props.total_pages]);
    
    // const pages_btn=props.total_pages.map((item,index)=>{
    //     if(index==5&&props.total_page_no>10){
    //         return <li><a href="#" >...</a></li>;
    //     } 
    //     else if(index>=6&&props.total_page_no>10)
    //     {
    //         return <li><a href="#" onClick={()=>{props.handlePagination(props.total_page_no-(10-index))}}>{props.total_page_no-(10-index)}</a></li>;
    //     }  
    //     else{
    //         return <li><a href="#" onClick={()=>{props.handlePagination(item)}}>{item}</a></li>;
    //     }

    //     console.log(item)
        
    // });    
    
    const handlePageChange =value=>{
        setActivePage(value);
        props.handlePagination(value,update_view);
    }
    const update_view = () =>{
        console.log("updateing view")
          forceUpdate();
    }

    const tutors = props.tutor_collection.map((item)=>{
        return (
        <Tutor handleProfile={props.handleProfile} profile_id={item.id} profile_pic={item.image} name={item.first_name} intro={item.brief_info} discription={item.bio} location={item.location}  tags={item.skills} price={item.hourly_rate} rating={item.rating}/>
        )
    })

    return (
        <div className="col-sm-9  prbutton4 msu">  
       <div className="tutors_div">
       {tutors}   
       </div>
       <div className="pagination_div">
       <ul className="pagination pagination-lg">
          {/* {pages_btn} */}
          <Pagination
          activePage={activePage}
          itemsCountPerPage={10}
          totalItemsCount={props.total_records}
          pageRangeDisplayed={10}
          onChange={handlePageChange}
        />
</ul>   
</div>
       
        </div>
    ); 
}
export default Tutors;