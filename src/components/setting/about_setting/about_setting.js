import React, { useState, useEffect } from "react";
import "./about_setting.css";
import axios from 'axios';
import constants from '../../../config';
const AboutSetting = props => {

    console.log(props.AboutDetails)
  const [aboutEdit, setAboutEdit] = useState(false);
  const [aboutSummary, setAboutSummary] = useState(props.AboutIntro);
  const [aboutDetails, setAboutDetials] = useState(props.AboutDetails);
  const [hourlyRate, setHourlyRate] = useState(props.hourly_rate);  
  const [role, setRole] = useState(props.role);
  
  const [briefIntro, setBriefIntro] = useState(aboutSummary);
  const [DetailedIntro, setDetailedIntro] = useState(aboutDetails);
  const [Hourly_Rate,SetHourlyRate] = useState(hourlyRate)

  const handleBriefIntro = e => {
    setBriefIntro(e.target.value);
  };

  const handleDetailedIntro = e => {
    setDetailedIntro(e.target.value);
  };


  const handleAboutEdit = () => {
    setAboutEdit(!aboutEdit);
  };
  const handleSave =async () => {
    setAboutSummary(briefIntro);
    setAboutDetials(DetailedIntro);
    setHourlyRate(Hourly_Rate);
    await axios.patch(constants.Base_URL+'v1/user/update_bio',{
        user:{
            bio:DetailedIntro,
            brief_info:briefIntro,
            hourly_rate:Hourly_Rate, 
        }
        
    }).then((res)=>{
        console.log(res);
    }); 
    setAboutEdit(!aboutEdit);
  };
  const handleCancel = () => {
      setBriefIntro(aboutSummary);
      setDetailedIntro(aboutDetails);
      SetHourlyRate(hourlyRate)
      setAboutEdit(!aboutEdit);
  }
  const handleHourlyRate = e => {
    SetHourlyRate(e.target.value);
  };
   const AboutEditDiv = (
    <div className="edit_form">
      <div className="inline_divs">
          <div className="element_div">
         <label>Brief Introduction:</label>
      <input value={briefIntro} type="text"  onChange={handleBriefIntro} />
      </div>
      <div className="element_div">
            <label>Hourly Rate:</label>
            <select onChange={handleHourlyRate} value={Hourly_Rate}>
              <option value="25">25</option>
              <option value="35">35</option>
              <option value="45">45</option>
              <option value="55">55</option>
              <option value="65">65</option>
              <option value="75">75</option>
              <option value="85">85</option>
              <option value="95">95</option>
              <option value="100">100</option>
            </select>
          </div>
    </div>
      <div className="element_div">
         <label>Detailed Introduction:</label>
      <textarea  value={DetailedIntro} onChange={handleDetailedIntro} />
      </div>
      
      
      <div className="footer_edit_form">
        <button onClick={handleSave} className="save">
          save
        </button>
        <button onClick={handleCancel} className="cancel">
          cancel
        </button>
      </div> 
    </div>
  );

  const AboutDiv = (
    <div>
      {role != "tutor" ? null : (  
        <div className="element_div std">
            <h3>Hourly Rate:</h3>   
            <h4>
              {hourlyRate ? hourlyRate : "click on edit to enter hourly rate"}
            </h4> 
          </div>
          )}
      
      <span className="titplus mbottom5">{aboutSummary}</span>
      <p className="casella_txt">{aboutDetails}</p>
      
    </div>
  );
  
  const Hourly_Rate_Edit=()=>{
     
        
  }
  return (
    <div>
      <div className="casella setting_box">
        <h1>
          About
          <button
            onClick={handleAboutEdit}
            className="btn btn-default btn-circle pull-right casella_btn"
          >
            <i className="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </h1>
        <hr className="h" />
        {aboutEdit?AboutEditDiv:AboutDiv}
        
      </div>
    
    
        
    </div>
  );
};
export default AboutSetting;
