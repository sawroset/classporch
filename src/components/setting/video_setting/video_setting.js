import React,{useState} from 'react';
import './video_setting.css';
import axios from 'axios';
import constants from '../../../config';
const VidoeSetting = props => {
    const [editVideo,setEditVideo] = useState(false);
    const [videoURL,setVideoURL] = useState(props.videoURL);
    const [video_url,setVideo_url] = useState("");

    const handleVidoeUrl =(e) =>{
      console.log(e.target.value)
         setVideo_url(e.target.value);
    }

    const VideoSave =async () => {
        let link="https://youtube.com/embed";
         
        let v_link=video_url.split("=");
        console.log(v_link[v_link.length-1]);
        link=link+"/"+v_link[v_link.length-1];
        console.log(link)
        
       await axios.patch(constants.Base_URL+'v1/user/update_profile_video',{
           user:{
               video_url:link
           }
       }).then((res)=>{
           console.log(res);    
       });
       setVideoURL(link);
       setEditVideo(!editVideo);
    }
    const VideoCancel = () => {
        setEditVideo(!editVideo);
    }
    const handleEditVideo= () =>{
        setEditVideo(!editVideo)
    }
    
  const VideoDivEdit = (
    <div className="edit_form element_div">
      
      <input type="text" onChange={handleVidoeUrl} placeholder="enter url" />
      
      <div className="footer_edit_form">
        <button onClick={VideoSave} className="save">
          save
        </button>
        <button onClick={VideoCancel} className="cancel">
          cancel
        </button>
      </div>
    </div>
  );

  const VideoDiv = (
    <iframe allowFullScreen="true" width="100%" height="100%" src={videoURL}></iframe>
  );
    return(
        <div>
        <div className="casella setting_box">
          <h1>
            Video
            <button
              onClick={handleEditVideo}
              className="btn btn-default btn-circle pull-right casella_btn"
            >
              <i className="fa fa-pencil" aria-hidden="true"></i>
            </button>
          </h1>
          <hr className="h" />
          {editVideo?VideoDivEdit:VideoDiv}
        </div>
      </div>
    )
}
export default VidoeSetting;