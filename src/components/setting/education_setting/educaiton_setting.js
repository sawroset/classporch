import React, { useState } from "react";
import "./education_setting.css";

import axios from "axios";
import constants from "../../../config";
const EducationSetting = props => {
  const [editEducations, setEditEducations] = useState(false);
  const [Educations, setEducations] = useState(props.educations);
  const [universityName, setUniversityName] = useState("");
  const [degreeName, setDegreeName] = useState("");
  const [educationfromDate, setEducationFromDate] = useState();
  const [educationToDate, setEducationToDate] = useState();
  const [verificationDocument, setVerificationDoc] = useState("");

  const handleEditEducations = () => {
    setEditEducations(!editEducations);
  };

  const handleUniversityName = e => {
    setUniversityName(e.target.value);
  };

  const handleDegreeName = e => {
    setDegreeName(e.target.value);
  };

  const handleEducationToDate = e => {
    setEducationToDate(e.target.value);
  };
  const handleEducationFromDate = e => {
    setEducationFromDate(e.target.value);
  };

  const handleVerificationDocFile = e => {
    console.log(e.target.files[0]);
    setVerificationDoc(e.target.files[0]);
  };

  const EducationSave = async e => {
    e.preventDefault();
    console.log(verificationDocument);
    

    const form_data= new FormData();

     form_data.append('verification_document',verificationDocument); 
     form_data.append('university_name',universityName);
     form_data.append('start_education',educationfromDate);
     form_data.append('finish_education',educationToDate);
   console.log(form_data.entries());  
   for(let item of form_data.entries()){
     console.log(item);
   }
    await axios.patch(constants.Base_URL+"v1/user/update_educations",form_data).then((res) => { 
        console.log("response ")  
        console.log(res);  
      }, 
      (err)=>{        
        console.log(err)
      }); 
  
    // let arr = [];
    // arr = Educations;
    // arr.push({
    //   start_education: educationfromDate,
    //   finish_education: educationToDate,
    //   university_name: universityName
    // });
    // setEducations(arr);
    // setEditEducations(false);
  };

  const EducationCancel = () => {
    setEditEducations(false);
  };

  const handleEditEducation = () => {
    setEditEducations(!editEducations);
  };

  const education = Educations.map(item => {
    return (
      <div className="col-sm-12 margin">
        <img
          src="/assets/dis1.png"
          className="img-responsive pull-left media-left"
          alt=""
        />
        <p className="titplus">{item.university_name}</p>
        <p className="tit2plus">{item.degree}</p>
        <p className="corplus">
          {item.start_education},{item.finish_education}
        </p>
      </div>
    );
  });
  const EducationDivEdit = (
    <form onSubmit={EducationSave} className="edit_form">
      <div className="inline_divs">
        <div className="element_div">
          <label>University Name:</label>
          <input
            type="text"
            onChange={handleUniversityName}
            placeholder="university name"
            required
          />
        </div>
        
      </div>
      <div className="inline_divs">
        <div className="element_div date_item date_left">
          <label>from date:</label>
          <input required type="date" onChange={handleEducationFromDate} />
        </div>
        <div className="element_div date_item date_right">
          <label>to date:</label>
          <input required type="date" onChange={handleEducationToDate} />
        </div>
      </div>
      <div className="inline_divs">
        <div className="element_div">
          <label>verification Documents:</label>
          <input type="file" required onChange={handleVerificationDocFile} />
        </div>
      </div>
      <div className="footer_edit_form">
        <button className="save">save</button>
        <button onClick={EducationCancel} className="cancel">
          cancel
        </button>
      </div>
    </form>
  );
  const EducationDiv = <div>{education}</div>;

  return (
    <div>
      <div className="casella setting_box">
        <h1>
          Education
          <button
            onClick={handleEditEducations}
            className="btn btn-default btn-circle pull-right casella_btn"
          >
            <i className="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </h1>
        <hr className="h" />
        {editEducations ? EducationDivEdit : EducationDiv}
      </div>
    </div>
  );
};
export default EducationSetting;
