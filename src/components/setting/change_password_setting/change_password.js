import React, { useState } from "react";
import "./change_password.css";
import axios from "axios";
import constants from "../../../config";

const ChangePassword = props => {
  const [password, setPassword] = useState("");
  const [new_password, setNewPassword] = useState("");
  const [confirm_new_password, setConfirmNewPassword] = useState("");
  const [modalVisible, setModalVisibility] = useState(false);
  const [apiResponse, setApiResponse] = useState();
  const [show, setShowHide] = useState(false);

  const handlePassword = e => {
    setPassword(e.target.value);
  };

  const handlePasswordShwo = () => {
    setShowHide(!show);
  };

  const handleNewPassword = e => {
    setNewPassword(e.target.value);
  };

  const handleConfirmNewPassword = e => {
    setConfirmNewPassword(e.target.value);
  };

  const handleSave = () => {
    axios
      .post(constants.Base_URL + "v1/user/change_password", {
        password: {
          old_password: password,
          new_password: new_password,
          confirm_new_password: confirm_new_password
        }
      })
      .then(res => {
        console.log(res);
        setPassword("");
        setNewPassword("");
        setConfirmNewPassword("");
        setApiResponse(JSON.stringify(res.data.response));
        setModalVisibility(true);
      });
  };

  console.log("modalVisible", modalVisible);
  return (
    <div className="change_password_setting">
      <h1 className="account-title">
        Change password
        <button
          className="btn btn-default btn-circle pull-right casella_btn"
          id="account_btn"
          onClick={handlePasswordShwo}
        >
          <i className="fa fa-pencil" aria-hidden="true"></i>
        </button>
      </h1>
      {show ? (
        <React.Fragment>
          <label>current password</label>
          <input
            value={password}
            onChange={handlePassword}
            type="password"
            placeholder="current password"
          />
          <label>new password</label>
          <input
            value={new_password}
            onChange={handleNewPassword}
            type="password"
            placeholder="new password"
          />
          <label>confirm new password</label>
          <input
            value={confirm_new_password}
            onChange={handleConfirmNewPassword}
            type="password"
            placeholder="confirm new password"
          />

          <button onClick={handleSave} className="save">
            save
          </button>
        </React.Fragment>
      ) : (
        <div className="hasSet">
          <strong>Password has been set</strong>
          <p>
            Choose a strong, unique password that’s at least 8 characters long
          </p>
        </div>
      )}

      {modalVisible && (
        <div className="change-password-modal modal fade in">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Change password confirmation</h4>
                <button
                  type="button"
                  className="close"
                  onClick={() => setModalVisibility(false)}
                >
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <h4>{apiResponse}</h4>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default ChangePassword;
