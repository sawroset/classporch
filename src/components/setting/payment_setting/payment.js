import React ,{useState} from 'react';
import './payment.css';
import {Elements, StripeProvider} from 'react-stripe-elements';
import Checkout from './stripe';
import constants from '../../../config';
// import { PayPalButton } from "react-paypal-button-v2";

const Payment = props => {
    const [selectedMethod,setSelectedMethod] = useState("stripe");
    const [stripeChecked,setStripeChecked] = useState(true);
    const [payPalChecked,setPayPalChecked] = useState(false);
    const stripe_div=  <StripeProvider apiKey={constants.Stripe_API_KEY}>
    <div className="example">
      <h1>Stripe </h1>
      <Elements>
        <Checkout />    
      </Elements> 
    </div>
  </StripeProvider>;

//   const paypal_div= <div className="paypal">
//   <PayPalButton
//     amount="0.01"
//     // options={
//     //   {disableCard:['visa','amex','mastercard','discover','paypalcredit']}
//     // }  
//     onSuccess={(details, data) => {
//       alert("Transaction completed by " + details.payer.name.given_name);

//       // OPTIONAL: Call your server to save the transaction
//       return fetch("/paypal-transaction-complete", {
//         method: "post",
//         body: JSON.stringify({
//           orderID: data.orderID
//         })
//       });
//     }}
//   />

//   </div> 
//  ;

const paypal_div=<div>commented</div>
const handleMethod = (e) => {
  let value=e.target.value;
  let checked=e;
  console.log(checked)
  if(value=="stripe"){
    if(stripeChecked==true){
      setStripeChecked(false);
      setPayPalChecked(false)
    }
    else{
      setStripeChecked(true);
      setPayPalChecked(false)
    }
    
  }
  else{
    if(payPalChecked==true){
      
      setStripeChecked(false);
      setPayPalChecked(false);
    }
    else{
      
      setStripeChecked(false);
      setPayPalChecked(true)
    }
    
    
  }
  setSelectedMethod(e.target.value);

};
const submit = async  () => {
  // console.log("akjsakjs")
  // let {token} = await props.stripe.createToken({name: "Name"});
  // console.log(token)
  // let response = await axios.post(constants.Base_URL+"v1/customer_token", {   
  //   method: "POST",
  //   headers: {"Content-Type": "text/plain"},
  //   body: token.id 
  // });

  // if (response.ok) console.log("Purchase Complete!")
};
return (
  <div className="payment_screen">

<button className="add_payment" data-toggle="modal" data-target="#myModal">add payment method</button>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        
        <div class="modal-header">
          <h4 class="modal-title">Payment Methods</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-body">
          <div className="inline_div">
           <label>Stripe:<input type="checkbox" checked={stripeChecked} onClick={handleMethod} value="stripe"/></label>
           {selectedMethod=="stripe"&&stripeChecked==true?stripe_div:null}
          </div>

          <div className="inline_div">
           <label>Paypal:<input type="checkbox" checked={payPalChecked} onClick={handleMethod} value="paypal"/></label>
           {selectedMethod=="paypal"&&payPalChecked==true?paypal_div:null} 
           
          </div>
          
          
       
        </div>
        
        
        <div class="modal-footer">
        <button class="btn_save" onClick={submit} data-dismiss="modal">save</button>
          
        </div>
        
      </div>
    </div>
  </div>
  

 
      </div>
);
};
   
export default Payment;