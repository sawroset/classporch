import React , {useState,useEffect} from "react";
import "./setting.css";
import SettingSideMenu from './side_menu';

import { useDispatch, useSelector } from "react-redux";
import { Route } from "react-router-dom";
import { 
  Navbar_clr_white,
  Navbar_auth,
  Navbar_clr_grey,
  Background_clr_white,
  Background_clr_grey
} from "../../actions/theme";
import {save_user_role} from '../../actions/authentication';
import PersonalInfo from "./personal_information_setting/personal_info";
import Payment from "./payment_setting/payment";
import ChangePassword from "./change_password_setting/change_password";
import { Elements, StripeProvider } from "react-stripe-elements";
import { contants } from "../../config";
import AboutSetting from "./about_setting/about_setting";
import EducationSetting from "./education_setting/educaiton_setting";
import SubjectsSetting from "./subjects_setting/subjects_setting";
import VidoeSetting from "./video_setting/video_setting";
import axios from 'axios';
import constants from '../../config';
import {Redirect} from 'react-router-dom';
const Setting = props => {  
  
  
 
  const dispatch = useDispatch();
  
  const [AboutIntro, setAboutIntro] = useState("");
  const [AboutDetails, setAboutDetails] = useState("");
  const [Subjects, setSubjects] = useState([]);
  const [Educations, setEducation] = useState([]);
  const [videoURL, setVideoURL] = useState("");
  const [allTags, setAllTags] = useState([]);
  const [role,setRole] = useState('');
  const [hourly_rate,setHourlyRate]=useState('');
  const auth=useSelector(state=>state.authentication_status);
  
  


  useEffect(() => {  
    window.scrollTo(0,0);
    if(auth!=true){
      props.history.push('/login')
    }
    else{
      dispatch(Background_clr_white());
      dispatch(Navbar_auth());
      axios.get(constants.Base_URL + "v1/profile").then(async (res) => {
        if(res.data.token=="Unauthorized access"){
          props.history.push('/login');
        }
        else{

        
        console.log(res.data.response);
        await setAboutIntro(res.data.response.brief_info);
        await setAboutDetails(res.data.response.bio);
        await setEducation(res.data.response.educations);
        await setVideoURL(res.data.response.video_url);
        await setSubjects(res.data.response.skills);
        await setRole(res.data.response.role);
        await setHourlyRate(res.data.response.hourly_rate);
        await axios.get(constants.Base_URL + "v1/skills").then(async res => {
          console.log("skills");
          console.log(res);
          await setAllTags(res.data.response);
        });
          
      }
  });
}
  return () => { 
    dispatch(Navbar_clr_grey());
    dispatch(Background_clr_grey()); 
  };
  }, []);
  
  const handleRoute = (val) => {
    props.history.push(val);
  }

  return (
    <div className=" setting_page">
      <div className="row hidden-xs">
        <div className="col-sm-12">
          <h1 className="mtop0">Settings page</h1>
        </div>

        {/* <!--<div className="col-sm-8"></div>--> */}
      </div>
 
      <br />
      <br />

      <div className="row tp">
        
       <SettingSideMenu role={useSelector(state=>state.user_role)} handleRoute={handleRoute}/>
        <div className="col-sm-8">
         
        
        <Route  
            path={`${props.match.url}/`}
            render={(props) => <Redirect to={`${props.match.url}/personalInfo`}  />}
          ></Route>
          <Route  
            path={`${props.match.url}/personalInfo`}
            render={(props) => <PersonalInfo {...props}  />}
          ></Route>
          <Route
            path={`${props.match.url}/payment`}
            render={(props) => <Payment {...props}  />}
          ></Route>
          <Route
            path={`${props.match.url}/changePassword`}
            render={(props) => <ChangePassword {...props}  />}
            
          ></Route>
          <Route
            path={`${props.match.url}/about`}
            render={(props) => <AboutSetting {...props} hourly_rate={hourly_rate} role={role} AboutIntro={AboutIntro} AboutDetails={AboutDetails} />}
            
          ></Route>
          <Route
            path={`${props.match.url}/education`}
            render={(props) => <EducationSetting {...props}  educations={Educations}/>}
            
          ></Route>
          <Route
            path={`${props.match.url}/subjects`}
            render={(props) => <SubjectsSetting {...props} AllSkills={allTags} subjects={Subjects} />}
            
          ></Route>
          <Route
            path={`${props.match.url}/video`}
            render={(props) => <VidoeSetting {...props} videoURL={videoURL}  />}
          ></Route>
        </div>
      </div>
    </div>
  );
};
export default Setting;
