import React ,{useEffect,useState} from "react";
import "./personal_info.css";
import csc from "country-state-city";

const CSC = props => {
    const [city, setCity] = useState(props.city);
  const [State, setState] = useState(props.State);
  const [country, setCountry] = useState(props.country);
  const [, forceUpdate] = useState();
    const [countries,serCountries] = useState(csc.getAllCountries());
    const [states,setStates] = useState(csc.getStatesOfCountry("1"));
    const [cities,setCities] = useState(csc.getCitiesOfState("1"));
    const getCountries = countries.map(item => {
      return <option value={item.id}>{item.name}</option>;
    });
   
    let getStates = states.map(item => {
      return <option value={item.id}>{item.name}</option>;
    });
    let getCities = cities.map((item) => {
    return  <option value={item.id}>{item.name}</option>;
    });

  
    const handleCountry = async e => {
        console.log(e.target.value);
        let temp_country = await csc.getCountryById(e.target.value);
        setCountry(temp_country.id);
        setStates(csc.getStatesOfCountry(temp_country.id));
        console.log(csc.getStatesOfCountry(temp_country.id)[0])
        setCities(csc.getCitiesOfState(csc.getStatesOfCountry(temp_country.id)[0].id));
        props.handleCountry(temp_country.id)
        forceUpdate();
      };
      const handleState = async e => {
        let temp_state=e.target.value;
        setState(temp_state);
        setCities(csc.getCitiesOfState(temp_state));
        props.handleState(temp_state)
        forceUpdate(); 
      }; 
    
      const handleCity = async e => {
        let temp_city = e.target.value;
        props.handleCity(temp_city)
        setCity(temp_city);
      };
  

  
  return (
    <div className="csc">
      <div className="inline_divs">
        <div className="element_div">
          <label>Country:</label>
          <select  onChange={handleCountry}>
            <option>{props.country}</option>
            {getCountries}
          </select>
        </div>
      </div>
      <div className="inline_divs">
        <div className="element_div">
          <label>State:</label>
          <select id="states_of_country"  onChange={handleState}>
            <option>{props.State}</option>
            {getStates}
          </select>
        </div>
      </div>
      <div className="inline_divs">
        <div className="element_div">
          <label>City:</label>
          <select  onChange={handleCity}>
            <option>{props.city}</option>
            {getCities}
          </select>
        </div>
      </div>
  </div>
  );
};
export default CSC; 
