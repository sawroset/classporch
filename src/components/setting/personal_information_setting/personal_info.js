import React, { useState, useEffect, useSelector, Fragment } from "react";
import axios from "axios";
import "./personal_info.css";
import constants from "../../../config";
import ct from "countries-and-timezones";
import csc from "country-state-city";
import CSC from "./country_state_city";
import TimezonePicker from "react-timezone";
import DropzoneInput from "../dropzone/dropzoneInput";
import Cropper from "cropperjs";
const PersonalInfo = props => {
  const [edit_account, setAccountEdit] = useState(false);
  const [edit_location, setLocationEdit] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [profile_pic, setProfilePic] = useState([]);
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState();

  const [gender, setGender] = useState("");
  const [timezone, setTimeZone] = useState("");
  const [parent_email, setParent_email] = useState("");
  const [parent_first_name, setParent_first_name] = useState("");
  const [parent_last_name, setParent_last_name] = useState("");
  const [parent_phoneNumber, setParentPhoneNumber] = useState("");
  const [grade, setGrade] = useState("");
  const [city, setCity] = useState("");
  const [State, setState] = useState("");
  const [country, setCountry] = useState("");
  const [role, setRole] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [language, setLanguage] = useState("");
  const [homeAddress, setHomeAddress] = useState("");

  const countries = csc.getAllCountries();
  let states = csc.getStatesOfCountry("1");
  let cities = csc.getCitiesOfState("1");
  const [imgPreview, setImagePreview] = useState(null);

  useEffect(() => {
    axios.get(constants.Base_URL + "v1/profile").then(async res => {
      if (res.data.token == "Unauthorized access") {
        props.history.push("/login");
      } else {
        console.log(res);
        await setFirstName(res.data.response.first_name);
        await setLastName(res.data.response.last_name);
        await setPhoneNumber(res.data.response.number);
        await setEmail(res.data.response.email);
        await setGender(res.data.response.gender);

        await setRole(res.data.response.role);
        await setCountry(res.data.response.country);
        await setCity(res.data.response.city);
        await setState(res.data.response.state);
        await setImagePreview(res.data.response.image);
      }
    });
  }, []);

  useEffect(() => {
    return () => {
      profile_pic.forEach(file => URL.revokeObjectURL(file.preview));
    };
  }, [profile_pic]);

  const handleEditAccount = e => {
    console.log(edit_account);
    setAccountEdit(!edit_account);
  };

  const handleEditLocation = e => {
    const timezones = ct.getAllTimezones();
    setLocationEdit(!edit_location);
  };

  const handleFirstName = e => {
    setFirstName(e.target.value);
  };

  const handleLastName = e => {
    setLastName(e.target.value);
  };

  const handleEmail = e => {
    setEmail(e.target.value);
  };

  const handlePhoneNumber = e => {
    console.log(e.target.value);
    setPhoneNumber(e.target.value);
  };

  const handleAccountSave = async () => {
    setAccountEdit(false);
    const form_data = new FormData();
    form_data.append("user[first_name]", firstName);
    form_data.append("user[last_name]", lastName);
    form_data.append("user[email]", email);
    form_data.append("user[phone_number]", phoneNumber);
    form_data.append("user[gender]", gender);
    form_data.append("user[profile_picture]", profile_pic);
    await axios.patch(constants.Base_URL + "v1/auth", form_data).then(res => {
      console.log("done!");
      console.log(res);
    });
  };

  const handleAccountCancel = () => {
    axios.get(constants.Base_URL + "v1/profile").then(async res => {
      console.log(res);
      await setFirstName(res.data.response.first_name);
      await setLastName(res.data.response.last_name);
      await setPhoneNumber(res.data.response.number);
      await setEmail(res.data.response.email);
      await setGender(res.data.response.gender);

      await setRole(res.data.response.role);
      await setCountry(res.data.response.country);
      await setCity(res.data.response.city);
      await setState(res.data.response.state);
      await setImagePreview(res.data.response.image);
    });
    setAccountEdit(false);
  };
  const handleLocationCancel = () => {
    setLocationEdit(false);
  };
  const handleLocationSave = async () => {
    setLocationEdit(false);
    console.log("saving info");
    console.log("gender:" + gender);
    console.log("state:" + State);
    let user = {
      country: country,
      city: city,
      state: State,
      timezone: timezone
    };
    console.log(user);
    await axios
      .patch(constants.Base_URL + "v1/auth", {
        user: user
      })
      .then(res => {
        console.log("done!");
        console.log(res);
      });
  };

  const handleTimeZone = e => {
    console.log(e);
    setTimeZone(e);
  };

  const handleGender = async e => {
    console.log(e.target.value);
    await setGender(e.target.value);
  };

  const handleProfilePic = async e => {
    let img = e.target.files[0];
    setImagePreview(URL.createObjectURL(img));
    await setProfilePic(img);
  };

  const handleCountry = value => {
    setCountry(csc.getCountryById(value).name);
  };
  const handleState = value => {
    setState(csc.getStateById(value).name);
  };
  const handleCity = value => {
    setCity(csc.getCityById(value).name);
  };
  const handleParentLastName = e => {
    setParent_last_name(e.target.value);
  };
  const handleParentFistName = e => {
    setParent_first_name(e.target.value);
  };
  const handleParentEmail = e => {
    setParent_email(e.target.value);
  };
  const handleParentPhoneNumber = e => {
    setParentPhoneNumber(e.target.value);
  };
  const handleGrade = e => {
    setGrade(e.target.value);
  };
  console.log(profile_pic);
  const AccountEditDiv = (
    <div className="edit_form">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6">
            <h4>Upload Image</h4>
            <DropzoneInput setfile={setProfilePic} />
          </div>

          <div className="col-md-6">
            <h4>Preview </h4>
            {profile_pic.length > 0 && (
              <img src={profile_pic[0].preview} style={{ height: "150px" }} />
            )}
          </div>
        </div>
      </div>
      <div className="inline_divs">
        <div className="element_div">
          <label>First Name:</label>
          <input
            value={firstName}
            onChange={handleFirstName}
            type="text"
            placeholder="first name"
          />
        </div>
        <div className="element_div">
          <label>Last Name:</label>
          <input
            value={lastName}
            onChange={handleLastName}
            type="text"
            placeholder="last name"
          />
        </div>
      </div>
      {false && role === "student" && (
        <div className="inline_divs">
          <div className="element_div">
            <label>Parent Phone Number:</label>
            <input
              value={parent_phoneNumber}
              onChange={handleParentPhoneNumber}
              type="number"
              placeholder="Parent phone number"
            />
          </div>
        </div>
      )}
      {role === "student" && (
        <div className="inline_divs">
          <div className="element_div">
            <label>Parent First Name:</label>
            <input
              value={parent_first_name}
              onChange={handleParentFistName}
              type="text"
              placeholder="Parent first name"
            />
          </div>
          <div className="element_div">
            <label>Parent Last Name:</label>
            <input
              value={parent_last_name}
              onChange={handleParentLastName}
              type="text"
              placeholder="Parent last name"
            />
          </div>
        </div>
      )}
      <div className="inline_divs">
        {role === "student" && (
          <div className="element_div">
            <label>Grade:</label>
            <input
              value={grade}
              onChange={handleGrade}
              type="number"
              placeholder="Grade"
            />
          </div>
        )}
        {role === "tutor" && (
          <div className="element_div">
            <label>Date of birth:</label>
            <input
              value={dateOfBirth}
              onChange={e => setDateOfBirth(e.target.value)}
              type="text"
              placeholder="Date of birth"
            />
          </div>
        )}
        <div className="element_div">
          <label>Phone Number:</label>
          <input
            value={phoneNumber}
            onChange={handlePhoneNumber}
            type="number"
            placeholder="phone number"
          />
        </div>
      </div>
      <div className="inline_divs">
        <div className="element_div">
          <label>Select gender:</label>
          <select value={gender} onChange={handleGender}>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </div>
        <div className="element_div ">
          <label>Email:</label>
          <input
            value={email}
            onChange={handleEmail}
            type="email"
            placeholder="email"
          />
        </div>
        {false &&
          role ===
            "student"(
              <div className="element_div">
                <label>Parent Email:</label>
                <input
                  value={parent_email}
                  onChange={handleParentEmail}
                  type="email"
                  placeholder="Parent email"
                />
              </div>
            )}
      </div>
      {role === "tutor" && (
        <div className="inline_divs">
          <div className="element_div ">
            <label>Language spoken:</label>
            <select
              value={language}
              onChange={e => setLanguage(e.target.value)}
            >
              <option value="English">English</option>
              <option value="German">German</option>
            </select>
          </div>
        </div>
      )}
      <div className="footer_edit_form">
        <button onClick={handleAccountSave} className="save">
          save
        </button>
        <button onClick={handleAccountCancel} className="cancel">
          cancel
        </button>
      </div>
    </div>
  );

  const AccountDiv = (
    <div>
      <div className="inline_divs">
        <div className="element_div">
          <h4>First Name</h4>
          <span>
            {" "}
            {firstName ? firstName : "click on edit to enter first name"}{" "}
          </span>
        </div>
        <div className="element_div">
          <h4>Last Name</h4>
          <span>
            {lastName ? lastName : "click on edit to enter last name"}{" "}
          </span>
        </div>
      </div>
      {role !== "student" ? null : (
        <div className="inline_divs">
          <div className="element_div">
            <h4>Parent First Name:</h4>
            <span>
              {parent_first_name
                ? parent_first_name
                : "click on edit to enter parent first name"}
            </span>
          </div>
          <div className="element_div">
            <h4>Parent Last Name:</h4>
            <span>
              {parent_last_name
                ? parent_last_name
                : "click on edit to enter parent last name"}
            </span>
          </div>
        </div>
      )}
      <div className="inline_divs">
        {role === "student" && (
          <div className="element_div">
            <h4>Grade</h4>
            <span>{grade ? grade : "click on edit to enter grade"}</span>
          </div>
        )}
        {role === "tutor" && (
          <div className="element_div">
            <h4>Date of Birth</h4>
            <span>
              {dateOfBirth
                ? dateOfBirth
                : "click on edit to enter date of birth"}
            </span>
          </div>
        )}
        <div className="element_div">
          <h4>Phone Number</h4>
          <span>
            {phoneNumber ? phoneNumber : "click on edit to enter phone number"}
          </span>
        </div>
      </div>
      {false && role === "student" && (
        <div className="inline_divs">
          <div className="element_div">
            <h4>Parent Email:</h4>
            <span>
              {parent_email
                ? parent_email
                : "click on edit to enter parent email"}
            </span>
          </div>
          <div className="element_div">
            <h4>Parent PhoneNumber:</h4>
            <span>
              {parent_phoneNumber
                ? parent_phoneNumber
                : "click on edit to enter phone number"}
            </span>
          </div>
        </div>
      )}
      <div className="inline_divs">
        <div className="element_div ">
          <h4>Gender</h4>
          <span>{gender ? gender : "click on edit to enter gender"} </span>
        </div>
        <div className="element_div">
          <h4>Email</h4>
          <span>{email ? email : "click on edit to enter email"} </span>
        </div>
      </div>
      {role === "tutor" && (
        <div className="inline_divs">
          <div className="element_div ">
            <h4>Language Spoken:</h4>
            <span>
              {language ? language : "click on edit to choose language spoken"}{" "}
            </span>
          </div>
        </div>
      )}
    </div>
  );

  // const EducationDiv = (

  // );
  const LocationEditDiv = (
    <div className="edit_form location">
      <div className="location">
        <CSC
          handleCountry={handleCountry}
          handleCity={handleCity}
          handleState={handleState}
          country={country}
          State={State}
          city={city}
        />
        <div className="inline_divs">
          <div className="element_div">
            <label>Timezone:</label>
            <TimezonePicker
              value={timezone}
              onChange={e => handleTimeZone(e)}
              className="timezone-picker"
              inputProps={{
                placeholder: "Select Timezone...",
                name: "timezone"
              }}
            />
          </div>
        </div>
        {role === "tutor" && (
          <div className="inline_divs">
            <div className="element_div">
              <label>Home address:</label>
              <input
                value={homeAddress}
                onChange={e => setHomeAddress(e.target.value)}
                type="text"
                placeholder="Home address"
              />
            </div>
          </div>
        )}
      </div>
      <div className="footer_edit_form">
        <button onClick={handleLocationSave} className="save">
          save
        </button>
        <button onClick={handleLocationCancel} className="cancel">
          cancel
        </button>
      </div>
    </div>
  );

  const LocationDiv = (
    <div>
      <h4>Country:</h4>
      <span>{country ? country : "click edit to set countyr"}</span>
      <br />
      <h4>State:</h4>
      <span>{State ? State : "click edit to set state"}</span>
      <br />
      <h4>City:</h4>
      <span>{city ? city : "click edit to set city"}</span>
      <br />
      <h4>Time zone:</h4>
      <span>{timezone ? timezone : "click edit to set timezone"}</span>
      <br />
      {role === "tutor" && (
        <Fragment>
          <label>Home address:</label>
          {homeAddress ? homeAddress : "click edit to set home address"}
          <br />
        </Fragment>
      )}
    </div>
  );

  return (
    <React.Fragment>
      <div className="casella setting_box">
        <h1 className="account-title">
          Account
          <button
            onClick={handleEditAccount}
            className="btn btn-default btn-circle pull-right casella_btn"
            id="account_btn"
          >
            <i className="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </h1>

        {edit_account ? AccountEditDiv : AccountDiv}
      </div>

      <br />
      <br />

      <div className="casella setting_box">
        <h1 className="account-title">
          Location
          <button
            onClick={handleEditLocation}
            className="btn btn-default btn-circle pull-right casella_btn"
            id="account_btn"
          >
            <i className="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </h1>
        {edit_location ? LocationEditDiv : LocationDiv}
      </div>
    </React.Fragment>
  );
};

export default PersonalInfo;
