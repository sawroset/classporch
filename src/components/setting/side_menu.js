import React , {useState,useEffect} from "react";
import {useSelector} from 'react-redux';
import "./setting.css";
const SettingSideMenu = props => {
  const [,forceUpdate] = useState('');
  const [role,setRole] = useState(props.role);
  useEffect(() => {
    forceUpdate();  
  }, [role]);
  const about = (
    <p
      onClick={() => {
        props.handleRoute(`/setting/about`);
      }}
      className={
        window.location.pathname == "/setting/about" ? "selez" : "noselz"
      }
    >
      About
    </p>
  );
  const subjects = (
    <p
      onClick={() => {
        props.handleRoute(`/setting/subjects`);
      }}
      className={
        window.location.pathname == "/setting/subjects" ? "selez" : "noselz"
      }
    >
      Subjects
    </p>
  );

  const education = (
    <p
      onClick={() => {
        props.handleRoute(`/setting/education`);
      }}
      className={
        window.location.pathname == "/setting/education" ? "selez" : "noselz"
      }
    >
      Education
    </p>
  );

  const video = ( 
    <p
      onClick={() => {
        props.handleRoute(`/setting/video`);
      }}
      className={
        window.location.pathname == "/setting/video" ? "selez" : "noselz"
      }
    >
      Video
    </p>
  );

  return (
    <div className="col-sm-4">
      <div className="hidden-xs">
        <p
          onClick={() => {
            props.handleRoute(`/setting/personalInfo`);
          }}
          className={
            window.location.pathname == "/setting/personalInfo"
              ? "selez"
              : "noselz"
          }
        >
          Personal Information
        </p>
        {props.role === "tutor" ? about : null}
        {false && subjects}
        {props.role === "tutor" ? education : null}
        {props.role === "tutor" ? video : null}

        <p
          onClick={() => {
            props.handleRoute(`/setting/payment`);
          }}
          className={
            window.location.pathname == "/setting/payment" ? "selez" : "noselz"
          }
        >
          Payment
        </p>
        <p
          onClick={() => {
            props.handleRoute(`/setting/changePassword`);
          }}
          className={
            window.location.pathname == "/setting/changePassword"
              ? "selez"
              : "noselz"
          }
        >
          Change Password
        </p>
      </div>

      <div className="form-group visible-xs">
        <select className="form-control" id="sel1">
          <option className="">Personal info</option> 
          <option className="">Payment</option>
          <option className="">Change Password</option>
          <option className="">Subjects</option> 
          {props.role == "tutor" ? <option className="">About</option> : null}
          
          {props.role == "tutor" ? <option className="">Education</option> : null}
          {props.role == "tutor" ? <option className="">Video</option> : null}
          
        </select>
      </div>
    </div>
  );
};
export default SettingSideMenu;
