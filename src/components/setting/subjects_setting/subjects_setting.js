import React,{useState,useEffect} from 'react';
import './subjects_setting.css';
import axios from 'axios';
import constants from '../../../config';
import Autosuggest from "react-autosuggest";
const SubjectsSetting = props => {

    const [editSubjects, setEditSubjects] = useState(false);
    const [Subjects, setSubjects] = useState(props.subjects);
    const [del_tags_list, setDelTagsList] = useState(props.subjects);
    
  
    const [subject, setSubject] = useState("");
    // const [newSubjects, setNewSubjects] = useState([]);
    
    const [suggestions, setSuggetions] = useState([]);
    
    const handleSubjectsEdit = () =>{
        setEditSubjects(!editSubjects);
    }
  
   
    const getTagSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
    
        return inputLength === 0
          ? []
          : 
          props.AllSkills.filter(
              lang => lang.name.toLowerCase().slice(0, inputLength) === inputValue 
            );
      };
      const getSuggestionValue = suggestion => {
        setSubject(suggestion.name); 
      };
    
      // Use your imagination to render suggestions.
      const renderSuggestion = suggestion => (
        <div
          onClick={() => {
            setSubject(suggestion.name);
            onSuggestionsClearRequested();
          }}
        >
          {suggestion.name}
        </div>
      );
      const onSuggestionsFetchRequested = ({ value }) => {
        setSuggetions(getTagSuggestions(value));
      };
    
      // Autosuggest will call this function every time you need to clear suggestions.
      const onSuggestionsClearRequested = () => {
        setSuggetions([]);
      };
      const handleEditSubjects = async () => {
        await setDelTagsList(props.tags);
        setEditSubjects(!editSubjects);
      };
      const handleSubject = e => {
        setSubject(e.target.value);
      };    
      const inputProps = {
        placeholder: "Type a programming language",
        value: subject,
        onChange: handleSubject
      };

     
      const addSubject =async () => {
            let arr=[]
            arr=del_tags_list;
           arr=await arr.filter(item=>{
                return item.name!=subject
            });   
            console.log(arr);
            arr.push({name:subject});
            console.log(arr)
            setDelTagsList(arr); 
            setSubject("");
        
      };
     
      const SubjectSave = async () => {
        let user = {
          skills: del_tags_list
        };
        await axios
          .patch(constants.Base_URL + "v1/user/update_skills", {
            user:{
                skills:del_tags_list
            }
          })
          .then(res => {});
          setSubjects(del_tags_list)
        setEditSubjects(false);
      };
      const SubjectCancel = async () => {
      await  setDelTagsList(props.subjects);
      await  setSubjects(props.subjects)
        setEditSubjects(false);  
      };
      const handleDelTags = async (deleted_tag) => {
        let deleted_tags = await del_tags_list.filter(item => {
          return item.name != deleted_tag;
        });
        console.log(deleted_tags);
        await setDelTagsList(deleted_tags); 
      };
    
      const delete_subjects = del_tags_list.map(item => {
        return (
          <span className="scuola2">
            {item.name}
            <button
              onClick={() => {  
                handleDelTags(item.name);
              }}
              className="btn btn-default btn-circle pull-right delete_btn"
            >
              <i className="fa fa-trash" aria-hidden="true"></i>
            </button>
          </span>
        );
      });
    const SubjectsEditDiv = (
        <div className="edit_form">
          <h5>subjects</h5>
          <div className="del_sub">{delete_subjects}</div>
   
            <div className="element_div">
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={onSuggestionsFetchRequested}
            onSuggestionsClearRequested={onSuggestionsClearRequested}
            renderSuggestion={renderSuggestion}
            inputProps={inputProps}
          />
            </div>     
           <button onClick={addSubject} className="add_subject_save">
             add skill
           </button>
    
           <div className="footer_edit_form">
             <button onClick={SubjectSave} className="save">
               save
             </button>
             <button onClick={SubjectCancel} className="cancel">
               cancel
             </button>
           </div>
         </div>
       );
       
    const subjects = Subjects.map(item => {
        return <span className="scuola2">{item.name}</span>;
      });
       const SubjectsDiv = <div>{subjects}</div>;
    
    return(
        <div>
        <div className="casella setting_box">
          <h1>
            Subjects
            <button
              onClick={handleSubjectsEdit}
              className="btn btn-default btn-circle pull-right casella_btn"
            >
              <i className="fa fa-pencil" aria-hidden="true"></i>
            </button>
          </h1>
          <hr className="h" />
          {editSubjects?SubjectsEditDiv:SubjectsDiv}
        </div>
      </div>
    )
}
export default SubjectsSetting;