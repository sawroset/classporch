import React, { useCallback } from "react";
import Dropzone, { useDropzone } from "react-dropzone";
import { FaUpload } from "react-icons/fa";

function DropzoneInput({ setfile }) {
  const onDrop = useCallback(
    acceptedFiles => {
      setfile(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );
    },
    [setfile]
  );
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: "image/*"
  });

  return (
    <div
      {...getRootProps()}
      className={"dropzone" + " " + (isDragActive && "dropzone--isActive")}
    >
      <input {...getInputProps()} />
      <FaUpload />
    </div>
  );
}

export default DropzoneInput;
