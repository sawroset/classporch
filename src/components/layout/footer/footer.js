import React from 'react';
import './footer.css';
import {Link} from 'react-router-dom';

const Footer = props => {
    return (
        <div className="row footsi">
            <div className="container">

                {/* <!--<div className="col-sm-1"></div>--> */}
 
                <div className="col-sm-3">
                 <Link to="/"><img src="/assets/logo.png" className="img-responsive" alt="" /></Link>    <br /><br />
                    <img src="/assets/fb.png" alt="" className="pleft20" />
                    <img src="/assets/tw.png" alt="" className="pleft20" />
                    <img src="/assets/ista.png" alt="" className="pleft20" />
                </div>

                <div className="col-sm-3">    
                   <Link to="/">Home</Link>  <br /><br />
                   <Link to="/about-us">About us</Link> <br /><br />
                   <Link to="/searchtutor">Search Tutors</Link><br /><br />
                   <Link to="">For Students</Link>
			</div>  

                <div className="col-sm-3">
                <Link to="">For Tutor</Link> <br /><br />
                    Pricing <br /><br />
                    <Link to="/faq">FAQ</Link><br /><br />
                    <Link to="/contact-us">Contact us</Link>
			</div>

                <div className="col-sm-3">
                    ESL Tutors <br /><br />
                    Biology Tutors <br /><br />
                    Adult Education <br /><br />
                    French Tutors
			</div>

                {/* <div className="col-sm-3">
                    <h3 className="mtop0 mbottom20">Newsletter</h3>
                    <p className="search">Enter email id</p>
                    <a href="#" className="rbutton3">SUBSCRIBE</a>
                </div> */}

                {/* <!--<div className="col-sm-1"></div>--> */}


                <br /><br />

                <div className="col-sm-12">
                    <br /><br />
                    <hr className="hrx" /> <br />
                    <p className="text-center">© 2019 ClassPorch. All Rights Reserved.</p>
                </div>

            </div>
        </div>

    );
}


export default Footer;