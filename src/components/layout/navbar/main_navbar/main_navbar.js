import React, { useEffect, useState, Fragment } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import "./main_navbar.css";
import { Log_out } from "../../../../actions/authentication";
import user from "../../../../user.svg";

const NavBar = props => {
  const dispatch = useDispatch();
  const auth_status = useSelector(state => state.authentication_status);
  const [user_role, setUserRole] = useState(
    useSelector(state => state.user_role)
  );
  const [, forceUpdate] = useState("");
  const logout = async () => {
    await dispatch(Log_out());
    props.history.push("/");
  };

  const GetLogin = (
    <li className="m_l">
      <Link to="/login" id="login-nav">
        <span className="co">Login</span>
      </Link>
    </li>
  );

  const GetSignup = (
    <li>
      <Link to="/signup">Sign Up</Link>
    </li>
  );

  const profile = e => {
    e.preventDefault();
    props.history.push("/profile");
  };

  const isTutor = useSelector(state => state.user_role) === "tutor";

  const authenticated_user_dd = (
    <li className="dropdown hidden-xs head_drop">
      <button
        className="btn btn-default dropdown-toggle"
        type="button"
        id="menu1"
        data-toggle="dropdown"
      >
        <img src={localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')).imageUrl:user} className="img-responsive img-circle pimg pull-left" />
        {localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).first_name+JSON.parse(localStorage.getItem('user')).last_name.split('')[0]}
        <span className="caret"></span>
      </button>
      {/* <button
        className="btn btn-default dropdown-toggle"
        type="button"
        id="menu1"
        data-toggle="dropdown"
      >
        <img src={user} className="img-responsive img-circle pimg pull-left" />
        User Name <span className="caret"></span>
      </button> */}
      <ul className="dropdown-menu" role="menu">
        <li className="active">
          <Link to="/setting">Setting</Link>
        </li>
        {isTutor && (
          <li className="active">
            <Link to="/profile">Profile</Link>
          </li>
        )}
        {!isTutor && (
          <Fragment>
            <li className="active">
              <Link to="/profile">FAQ</Link>
            </li>
            <li className="active">
              <Link to="/profile">Help</Link>
            </li>
          </Fragment>
        )}
        <li className="active">
          <a onClick={logout}>Logout</a>
        </li>
      </ul>
    </li>
  );

  const isAuth = useSelector(state => state.authentication_status);
  const userRole = useSelector(state => state.user_role);

  return (
    <div className="container_menu">
      <nav className={useSelector(state => state.Navbar)}>
        {/* <!-- Brand and toggle get grouped for better mobile display --> */}
        <div className="navbar-header">
          <button
            type="button"
            data-target="#navbarCollapse"
            data-toggle="collapse"
            className="navbar-toggle"
          >
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <Link to="/" className="navbar-brand">
            <img src="/assets/logo.png" className="img-responsive" alt="" />
          </Link>
        </div>
        {/* <!-- Collection of nav links and other content for toggling --> */}
        <div id="navbarCollapse" className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            {useSelector(state => state.authentication_status) ==
            true ? null : (
              <li className="m_l">
                <Link to="/forStudent">For Students</Link>
              </li>
            )}
            {useSelector(state => state.authentication_status) ==
            true ? null : (
              <li>
                <Link to="/forTutor">For Tutors</Link>
              </li>
            )}
            {isAuth === true && userRole === "student" && (
              <Fragment>
                <li className="search-tutors">
                  <div className="input-group input-group-sm">
                    <input
                      onChange={props.handleChange}
                      type="text"
                      name="search"
                      className="search_box form-control textform"
                      placeholder="Search Turos"
                      id="search_form"
                    />
                    <span className="input-group-addon">
                      <i className="fa fa-search" aria-hidden="true" />
                    </span>
                  </div>
                </li>
                <li>
                  <i className="fa fa-commenting" aria-hidden="true" />
                </li>
                <li>
                  <i className="fa fa-bell" aria-hidden="true" />
                </li>
              </Fragment>
            )}
            {useSelector(state => state.authentication_status) == true
              ? authenticated_user_dd
              : null}
            {auth_status == true || window.location.pathname == "login"
              ? null
              : GetLogin}
            {auth_status == true || window.location.pathname == "signup"
              ? null
              : GetSignup}
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default withRouter(NavBar);
