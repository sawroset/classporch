import React, { useEffect, useState } from "react";
import "./profile.css";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import {
  Navbar_clr_white,
  Navbar_clr_grey,
  Background_clr_white,
  Background_clr_grey
} from "../../actions/theme";
import Profile_info from "../ui_components/profile_info/profile_info";
import ProfileThumbnails from "../ui_components/profile_thumbnails/profile_thumbnails";
import Ratings_and_Reviews from "../ui_components/ratings_and_review/ratings_and_review";
import General_Availability from "../ui_components/generail_availability/general_availability";
import constants from "../../config";
const Profile = props => {
  if (
    useSelector(state => state.authentication_status) === false &&
    window.location.pathname === "/profile"
  ) {
    props.history.push("/login");
  }
  console.log(props);

  const [AboutIntro, setAboutIntro] = useState("");
  const [AboutDetails, setAboutDetails] = useState("");
  const [Subjects, setSubjects] = useState([]);
  const [profilePic, setProfilePic] = useState("");
  const [Educations, setEducation] = useState([]);
  const [videoURL, setVideoURL] = useState("");
  const [allTags, setAllTags] = useState([]);
  const [temp_auth, setTemp_auth] = useState(
    useSelector(state => state.authentication_status)
  );

  useEffect(() => {
    window.scrollTo(0, 0);
    if (window.location.pathname === "/profile") {
      axios.get(constants.Base_URL + "v1/profile").then(async res => {
        if (res.data.token == "Unauthorized access") {
          props.history.push("/login");
        } else {
          console.log(res.data.response);
          await setAboutIntro(res.data.response.brief_info);
          await setAboutDetails(res.data.response.bio);
          await setEducation(res.data.response.educations);
          await setVideoURL(res.data.response.video_url);
          await setSubjects(res.data.response.skills);
          await setProfilePic(res.data.response.image);
        }
      });
    } else {
      console.log(props.match.params.id);
      axios
        .get(
          constants.Base_URL + "v1/user/" + props.match.params.id + "/profile"
        )
        .then(async res => {
          if (res.data.token == "Unauthorized access") {
            props.history.push("/login");
          } else {
            await setAboutIntro(res.data.response.brief_info);
            await setAboutDetails(res.data.response.bio);
            await setEducation(res.data.response.educations);
            await setVideoURL(res.data.response.video_url);
            await setSubjects(res.data.response.skills);
            await setProfilePic(res.data.response.image);
          }
        });
    }

    return () => {
      dispatch(Navbar_clr_grey());
      dispatch(Background_clr_grey());
    };
  }, []);

  const handleAboutSave = (intro, details) => {
    setAboutIntro(intro);
    setAboutDetails(details);
  };
  const about = {
    intro: AboutIntro,
    discription: AboutDetails,
    handleSave: handleAboutSave
  };
  const profile_info = {
    name: "Grigor D.",
    price: "3.45",
    location: "Hilton, England, United Kingdom",
    rating: "2"
  };

  const education = Educations;

  const levels = [
    "Elementary",
    "Middle School",
    "High School",
    "College/University"
  ];
  const subjects = Subjects;

  const total_reviews = 38;
  const ratings = [
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 2
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 5
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 4
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 2
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 3
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 5
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 1
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 2
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 3
    },
    {
      name: "John Doe",
      role: "student",
      comments: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
      date: "8 April",
      profile_pic: "/assets/tipo2.jpg",
      ratting: 4
    }
  ];
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(Navbar_clr_white());
    dispatch(Background_clr_white());
    return () => {
      dispatch(Navbar_clr_grey());
      dispatch(Background_clr_grey());
    };
  }, []);

  return (
    <div>
      <Profile_info profilePic={profilePic} profile_info={profile_info} />
      <br />
      <br />
      <ProfileThumbnails
        education={education}
        about={about}
        tags={subjects}
        video_url={videoURL}
        AllSkills={allTags}
      />
      <br />
      <br />
      <div className="container">
        <Ratings_and_Reviews ratings={ratings} />
        <br />
        <br />
        <General_Availability />
        <br />
        <br />
      </div>
    </div>
  );
};

export default Profile;
