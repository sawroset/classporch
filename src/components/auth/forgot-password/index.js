import React from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { Background_clr_white } from '../../../actions/theme';
import { Navbar_hide, Navbar_clr_grey } from '../../../actions/theme';
import { Background_clr_grey } from '../../../actions/theme';

import constants from '../../../config';
import './style.css';
const ForgotPassword = props => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    dispatch(Background_clr_white());
    dispatch(Navbar_hide());
    return () => {
      dispatch(Background_clr_grey());
      dispatch(Navbar_clr_grey());
    };
  }, []);

  if (useSelector(state => state.authentication_status) == true) {
    return <Redirect to="/setting/personalInfo" />;
  }
  const handleEmail = e => {
    setEmail(e.target.value);
  };

  const submitForm = e => {
    e.preventDefault();
    setLoading(true);
    axios
      .post(constants.Base_URL + 'v1/user/forgot_password', {
        user: {
          email
        }
      })
      .then(async res => {
        setLoading(false);
        if (res.data.response.error) {
          console.log('error:' + res.data.response.error);
          setError(res.data.response.error);
          document.getElementById('err_btn').click();
        } else if (res.status == 200) {
          props.history.push('/login?forgot-password=true');
        }
      });
  };

  return (
    <div className="container login_component">
      <div className="row">
        {/* <!--<div className="col-sm-2"></div>--> */}

        <div className="col-sm-offset-3 col-sm-6">
          <h1 className="text-center login_title">FORGOT PASSWORD</h1>
          <hr className="hx" /> <br />
          <form
            onSubmit={submitForm}
            name="forgotPassword"
            method="post"
            className="form-signin form-group cdiv container"
          >
            <input
              onChange={handleEmail}
              type="email"
              className="cinput"
              name="Email"
              placeholder="Email"
              required
            />
            <br />
            <div className="row">
              <div className="col-sm-6">
                <Link className="black" to="/login">
                  Login?
                </Link>
              </div>
            </div>
            <br />
            <button
              href="#"
              className={`sbutton img-rounded center-block ${loading &&
                'loading'}`}
              disabled={loading}
            >
              <span className="scon">
                {loading ? 'In progress...' : 'Forgot Password'}
              </span>
            </button>
            <br />
          </form>
        </div>

        {/* <!--<div className="col-sm-2"></div>--> */}
      </div>

      <button
        id="err_btn"
        data-toggle="modal"
        data-target="#err_Modal"
      ></button>

      <div id="err_Modal" className="modal fade">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Error Message</h4>
              <button type="button" className="close" data-dismiss="modal">
                &times;
              </button>
            </div>
            <div className="modal-body">
              <h4>{error}</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
