import React, { useState, useEffect } from 'react';

import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { Background_clr_white } from '../../../actions/theme';
import { Navbar_hide, Navbar_clr_grey } from '../../../actions/theme';
import { Background_clr_grey } from '../../../actions/theme';

import constants from '../../../config';
export default props => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [error, setError] = useState('');
  const [errorSpan, setErrorSpan] = useState(undefined);
  useEffect(() => {
    dispatch(Background_clr_white());
    dispatch(Navbar_hide());
    return () => {
      dispatch(Background_clr_grey());
      dispatch(Navbar_clr_grey());
    };
  }, []);
  const handlePasswordUpdate = e => {
    e.preventDefault();
    console.log(
      constants.Base_URL +
        'v1/user/update_password_token/' +
        props.match.params.token
    );
    if (
      newPassword.trim() &&
      confirmPassword.trim() &&
      newPassword === confirmPassword
    ) {
      setLoading(true);
      axios
        .put(
          constants.Base_URL +
            `v1/update_password_token/${props.match.params.token}`,
          {
            password: {
              new_password: newPassword,
              password_confirmation: confirmPassword
            }
          }
        )
        .then(async res => {
          setLoading(false);
          if (res.data && res.data.status !== 200) {
            setError(res.data.message);
            document.getElementById('err_btn').click();
          } else if (res.data.status == 200) {
            props.history.push('/login?update-password=true');
          }
        });
    } else {
      setErrorSpan('Both fields should be same & non-empty!');
    }
  };
  return (
    <div className="container login_component">
      <div className="row">
        {/* <!--<div className="col-sm-2"></div>--> */}
        <div className="col-sm-offset-3 col-sm-6">
          <div className="change_password_setting">
            <h1>Update password</h1>
            {errorSpan && <div style={{ color: 'red' }}>{errorSpan}</div>}
            <label>New password*</label>
            <input
              value={newPassword}
              onChange={({ target }) => {
                setNewPassword(target.value);
                setErrorSpan(undefined);
              }}
              type="password"
              placeholder="new password"
              required
            />
            <label>Confirm New password*</label>
            <input
              value={confirmPassword}
              onChange={({ target }) => {
                setConfirmPassword(target.value);
                setErrorSpan(undefined);
              }}
              type="password"
              placeholder="confirm new password"
              required
            />

            <button
              onClick={handlePasswordUpdate}
              className={`save ${loading && 'loading'}`}
              disabled={loading}
            >
              {loading ? 'In progress...' : 'Save'}
            </button>
          </div>
        </div>
      </div>

      <button
        id="err_btn"
        data-toggle="modal"
        data-target="#err_Modal"
      ></button>

      <div id="err_Modal" className="modal fade">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Error Message</h4>
              <button type="button" className="close" data-dismiss="modal">
                &times;
              </button>
            </div>
            <div className="modal-body">
              <h4>{error}</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
