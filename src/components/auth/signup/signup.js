import React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import axios from "axios";

import { Background_clr_white } from "../../../actions/theme";
import { Background_clr_grey } from "../../../actions/theme";
import { Navbar_hide, Navbar_clr_grey } from "../../../actions/theme";
import constants from "../../../config";
import { login } from "../../../actions/authentication";
import { FaFacebookSquare, FaGoogle, FaArrowLeft } from "react-icons/fa";

import "../login/login.css";
import "./signup.css";

const Signup = props => {
  const [Fname, setFname] = useState("");
  const [Lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirm_password, setConfirmPassword] = useState("");
  const [selectedRole, setSelectedRole] = useState("");
  const [errors, setErrors] = useState([]);
  const [modal, setModal] = useState(false);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(Background_clr_white());
    dispatch(Navbar_hide());

    return () => {
      dispatch(Background_clr_grey());
      dispatch(Navbar_clr_grey());
    };
  }, [dispatch]);

  if (useSelector(state => state.authentication_status) === true) {
    return <Redirect to="/setting/personalInfo" />;
  }

  const handleEmail = e => {
    setEmail(e.target.value);
  };
  
  const handleShowHideModal = () => {
    setModal(!modal);
  };

  const handlePassword = e => {
    setPassword(e.target.value);
  };

  const handleConfirmPassword = e => {
    setConfirmPassword(e.target.value);
  };
  const handleFname = e => {
    setFname(e.target.value);
  };
  const handleLname = e => {
    setLname(e.target.value);
  };

  const selectRole = async value => {
    await setSelectedRole(value);
  };

  const submitForm = e => {
    e.preventDefault();
    axios
      .post(constants.Base_URL + "v1/user/init", {
        user: {
          email: email,
          first_name: Fname,
          last_name: Lname,
          password: password,
          password_confirmation: confirm_password,
          role: selectedRole
        }
      })
      .then(async res => {
        
        if (res.data.response.error) {
          setErrors(res.data.response.error);
        } else if (res.data.meta.status === 201) {
          await localStorage.setItem("auth_token",res.data.response.auth_token);
          await localStorage.setItem('user', JSON.stringify({
            email: email,
            last_name: Lname,
            first_name: Fname,
            imageUrl: null
          }));
          await dispatch(login());
          props.history.push("/setting/personalInfo");
        }
      });
  };

  const responseGoogle = async response => {
    await axios.post(constants.Base_URL + "v1/user/init", {
        user: {
          provider: "google",
          email: response.profileObj.email,
          last_name: response.profileObj.familyName,
          first_name: response.profileObj.givenName,
          image: response.profileObj.imageUrl,
          role: selectedRole
        }
      })
      .then(async res => {
        console.log(res.data);
        
        if(res.data.meta.code === 201) {
          localStorage.setItem('user', JSON.stringify({
            email: response.profileObj.email,
            last_name: response.profileObj.familyName,
            first_name: response.profileObj.givenName,
            imageUrl: response.profileObj.imageUrl
          }));
          await localStorage.setItem( "auth_token", res.data.response.auth_token);
          await dispatch(login());
          props.history.push("/setting/personalInfo");
        }else if (res.data.response.error) {
          setErrors(res.data.response.error);
        }
      });
  };

  const responseFacebook = async response => {
    // if(response.status == "unknown") {
    //   return setErrors("Unable to login through facebook")
    // }
    console.log("Facebook",response);
    
    // await axios
    //   .post(constants.Base_URL + 'v1/user/init', {
    //     user: {
    //       provider: 'facebook',
    //       role: selectedRole,
    //       email: email,
    //       last_name: response.name.split(' ')[1],
    //       first_name: response.name.split(' ')[0],
    //       image: response.picture.data.url,
    //       // access_token: response.accessToken
    //     }
    //   })
    //   .then(async res => {
    //     console.log(res);
        
    //     if(res.data.meta.code === 201) {
    //       console.log("success");
    //       // localStorage.setItem('user', JSON.stringify({
    //       //   email: response.profileObj.email,
    //       //   last_name: response.name.split(' ')[1],
    //       //   first_name: response.name.split(' ')[0],
    //       //   imageUrl: response.picture.data.url
    //       // }));
    //       // await localStorage.setItem( "auth_token", res.data.response.auth_token);
    //       // await dispatch(login());
    //       // props.history.push("/setting/personalInfo");
    //     }else if (res.data.response.error) {
    //       setErrors(res.data.response.error);
    //     }
    //   });




    // try {
    //   console.log(response);
    //   const res = await axios.post(constants.Base_URL + "v1/user/init", {
    //     user: {
    //       provider: "facebook",
    //       role: selectedRole,
    //       email: response.email,
    //       access_token: response.accessToken
    //     }
    //   });
    //   console.log(res);
    //   if (res.data.response.error) {
    //     console.log("HERE", res.data.response.error);
    //     const err = res.data.response.error;
    //     console.log(err);
    //     setErrors(err);
    //     // document.getElementById("err_btn").click();
    //     return true;
    //   } else if (res.data.status === 200) {
    //     await localStorage.setItem("auth_token", res.data.response.auth_token);
    //     await dispatch(login());
    //     props.history.push("/setting/personalInfo");
    //   }
    // } catch (error) {
    //   console.log(error);
    // }
  };

  const roleDiv = (
    <div className="roles">
      <span
        className={selectedRole === "student" ? "selected role" : "role"}
        onClick={() => selectRole("student")}
      >
        student
      </span>
      <span
        className={selectedRole === "tutor" ? "selected role" : "role"}
        onClick={() => selectRole("tutor")}
      >
        tutor
      </span>
    </div>
  );

  const FormDiv = (
    <div>
      {!modal ? (
        <React.Fragment>
          <FacebookLogin
            appId={constants.FB_APP_ID}
            callback={responseFacebook}
            fields="name,email,picture"
            render={renderProps => (
              <button className=" facebook" onClick={renderProps.onClick}>
                <FaFacebookSquare className="mr-5" />
                facebook
              </button>
            )}
          />

          <GoogleLogin
            clientId={constants.GOOGLE_Client_ID}
            render={renderProps => (
              <span
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
                className=" google"
                id="sb_btn "
              >
                <FaGoogle className="mr-5" />
                google
              </span>
            )}
            buttonText="Login"
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={"single_host_origin"}
          />
          <div className="divider">
            <span>Or sign up with email</span>
          </div>
          <button
            type="button"
            id="sign-up-gmail"
            onClick={handleShowHideModal}
          >
            Sign up with Email
          </button>
        </React.Fragment>
      ) : null}

      {modal ? (
        <React.Fragment>
          <button onClick={handleShowHideModal} className="back-signUp">
            <FaArrowLeft />
            Back
          </button>
          <input
            onChange={handleFname}
            type="text"
            className="cinput"
            name="#"
            placeholder="FirstName"
            required
            id="form_signup-input"
          />
          <input
            onChange={handleLname}
            type="text"
            className="cinput"
            name="#"
            placeholder="Last Name"
            required
            id="form_signup-input"
          />
          <input
            onChange={handleEmail}
            type="text"
            className="cinput"
            name="#"
            placeholder="Email"
            required
            id="form_signup-input"
          />
          <br />
          <input
            onChange={handlePassword}
            type="password"
            className="cinput"
            name="#"
            placeholder="Password"
            required
            id="form_signup-input"
          />
          <br />
          <input
            onChange={handleConfirmPassword}
            type="password"
            className="cinput"
            name="#"
            placeholder="Confirm Password"
            id="form_signup-input"
            required
          />

          <button className="btn-form">
            <span className="scon">SIGN UP</span>
          </button>
        </React.Fragment>
      ) : null}
    </div>
  );
  return (
    <div className="row signup_component">
      <div className="back">
        <div className="container">
          {/* <!--<div className="col-sm-4"></div>--> */}

          <div className="col-sm-12 text-center" id="sign-up-form">
            <h1 className="signup_title">Create an Account</h1>
            <h4 className="error">{errors}</h4>
            <form
              onSubmit={submitForm}
              name="#"
              method="post"
              className="form-signin form-group cform"
            >
              {selectedRole ? FormDiv : roleDiv}

              <p
                onClick={() => {
                  props.history.push("/login");
                }}
                className="text-left pleft5"
              >
                <Link to="/login" className="black">
                  Already have an account? <span className="alr">Log in</span>
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Signup;
