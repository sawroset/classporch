import React from "react";
import "./login.css";
import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { Background_clr_white } from "../../../actions/theme";
import { Navbar_hide, Navbar_clr_grey } from "../../../actions/theme";
import { Background_clr_grey } from "../../../actions/theme";
import {
  save_auth_user_info,
  login,
  save_user_role
} from "../../../actions/authentication";
import axios from "axios";
import constants from "../../../config";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import { Link, Redirect } from "react-router-dom";
import { FaFacebookSquare, FaGoogle } from "react-icons/fa";

const Login = props => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [remember_me, setRemember_me] = useState(false);
  const [message, setMessage] = useState("");
  const [messageTitle, setMessageTitle] = useState("Error Message");

  const params = new URLSearchParams(props.location.search);
  const forgotPasswordSuccess = params.get("forgot-password");
  const updatePassowrd = params.get("update-password");

  useEffect(() => {
    dispatch(Background_clr_white());
    dispatch(Navbar_hide());
    if (forgotPasswordSuccess) {
      setMessageTitle("Forgot Passowrd");
      setMessage("A link has been sent to your email account!");
      // document.getElementById("err_btn").click();
    }
    if (updatePassowrd) {
      setMessageTitle("Update Passowrd");
      setMessage("Password has been updated.");
      // document.getElementById("err_btn").click();
    }
    return () => {
      dispatch(Background_clr_grey());
      dispatch(Navbar_clr_grey());
    };
  }, [forgotPasswordSuccess, updatePassowrd]);

  if (useSelector(state => state.authentication_status) == true) {
    return <Redirect to="/setting/personalInfo" />;
  }
  const handleEmail = e => {
    setEmail(e.target.value);
  };

  const handlePassword = e => {
    setPassword(e.target.value);
  };

  const handleRemember_me = async e => {
    await setRemember_me(!remember_me);
  };

  const submitForm = e => {
    e.preventDefault();
    axios.post(constants.Base_URL + "v1/auth/sign_in", {
        user: {
          email: email,
          password: password
        }
      })
      .then(async res => {
        if (res.data.response.error) {
          setMessage(res.data.response.error);
        } else if (res.data.meta.code == 201) {
          localStorage.setItem("auth_token", res.data.response.auth_token);
          localStorage.setItem('user', JSON.stringify({
            email: res.data.response.email,
            last_name: res.data.response.first_name,
            first_name: res.data.response.last_name,
            imageUrl: res.data.response.image
          }));
          await dispatch(login());
          await dispatch(save_user_role(res.data.response.role));
          await dispatch(save_auth_user_info(res.data.response));
          props.history.push("/setting/personalInfo");
        }
      });
  };

  const responseGoogle = async response => {
    await axios.post(constants.Base_URL + "v1/auth/sign_in", {
        user: {
          provider: "google",
          email: response.profileObj.email
        }
      })
      .then(async res => {
        
        if (res.data.response.error) {
          setMessage(res.data.response.error);
        } else if (res.data.meta.code == 201) {
          localStorage.setItem("auth_token", res.data.response.auth_token);
          localStorage.setItem('user', JSON.stringify({
            email: res.data.response.email,
            last_name: res.data.response.first_name,
            first_name: res.data.response.last_name,
            imageUrl: res.data.response.image
          }));
          await dispatch(login());
          await dispatch(save_user_role(res.data.response.role));
          await dispatch(save_auth_user_info(res.data.response));
          props.history.push("/setting/personalInfo");
        }
        // console.log("SOMETHING ELSE", res);
        // if (res.data.response.auth_token) {
        //   localStorage.setItem("auth_token", res.data.response.auth_token);
        //   await dispatch(login());
        //   props.history.push("/setting/personalInfo");
        // }
      });
  };

  const responseFacebook = async response => {
    console.log(response);
    await axios
      .post(constants.Base_URL + "v1/user/init", {
        user: {
          provider: "facebook",
          email: response.email,
          access_token: response.accessToken
        }
      })
      .then(async res => {
        console.log(res);
        if (res.data.response.error) {
          console.log("HERE", res.data.response.error);
          const err = res.data.response.error;
          console.log(err);
          setMessage(err);
          // document.getElementById("err_btn").click();
          return true;
        } else if (res.data.status === 200) {
          await dispatch(login());
          props.history.push("/setting/personalInfo");
        }
      });
  };
  return (
    <div className="container login_component">
      <div className="row">
        {/* <!--<div className="col-sm-2"></div>--> */}

        <div className="form-content">
          <h1 className="text-center login_title">Log in</h1>

          <form
            onSubmit={submitForm}
            name="#"
            method="post"
            className="form-signin form-group cdiv container"
          >
            <FacebookLogin
              appId={731447330655528}
              fields="name,email,picture"
              callback={responseFacebook}
              render={renderProps => (
                <button className="facebook" onClick={renderProps.onClick}>
                  <FaFacebookSquare /> facebook
                </button>
              )}
            />

            <GoogleLogin
              clientId={constants.GOOGLE_Client_ID}
              render={renderProps => (
                <span
                  onClick={renderProps.onClick}
                  disabled={renderProps.disabled}
                  className=" google"
                >
                  <span>
                    <FaGoogle />
                  </span>
                  {/* <img className="g_logo" src="/assets/google_logo.png" /> */}
                  google
                </span>
              )}
              buttonText="google"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={"single_host_origin"}
            />

            <h4 className="error">{message}</h4>
            {/* <div className="divider">
              <span>Or Login with email</span>
            </div> */}
            <input
              onChange={handleEmail}
              type="email"
              className="form_input"
              name="#"
              placeholder="Email"
              required
            />

            <input
              onChange={handlePassword}
              type="password"
              className="form_input"
              name="#"
              placeholder="Password"
              required
            />

            <Link
              className="black"
              className="forget-pass"
              to="/forgot-password"
            >
              Forgot password?
            </Link>

            <button href="#" className="form-btn">
              login
            </button>

            <span className="account">
              Don't have Account?
              <Link className="black" to="/signup">
                Sign up
              </Link>
            </span>
          </form>
        </div>

        {/* <!--<div className="col-sm-2"></div>--> */}
      </div>

      {/* <button
        id="err_btn"
        data-toggle="modal"
        data-target="#err_Modal"
      ></button>

      <div id="err_Modal" className="modal fade">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">{messageTitle}</h4>
              <button type="button" className="close" data-dismiss="modal">
                &times;
              </button>
            </div>
            <div className="modal-body">
              <h4>{message}</h4>
            </div>
          </div>
        </div>
      </div> */}
    </div>
  );
};

export default Login;
