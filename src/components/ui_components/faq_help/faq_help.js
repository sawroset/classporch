import React from 'react';
import './faq_help.css';

const Faq_Help = props =>{
    const hadnleclick=()=>{
        props.handleRoute(props.url);
    }
return (

<div  onClick={hadnleclick} className="col-sm-3 scelta pscelta text-center">	
<div className="item">
    <img src={props.img_path} className="img-responsive center-block img_help" alt=""/> <br/>
   <span className="title">  {props.title} </span>
   </div>				
</div>  
);
}

export default Faq_Help;