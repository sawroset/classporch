import React from 'react';
import './contact_info.css';


const ContactInfo = props =>{

    return (
		<div className="row">
			<div className="col-sm-1"></div>
			
			<div className="col-sm-10 contx">
				<img src={props.img_path} className="img-responsive pull-left ptop10 media-left" alt=""/>
				<p className="backtitle">{props.title}:</p> 
				<p className="rientro">{props.discription}</p>
			</div>
			
			<div className="col-sm-1"></div>
		</div>
    );
}

export default ContactInfo;