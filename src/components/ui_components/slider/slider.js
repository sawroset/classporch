import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./slider.css";
const Sliders = props => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  };
  return (
    <React.Fragment>
      <h1 id="instructor" className="text-center">
        Meet Our Instructor
      </h1>
      <div className="slider container about_slider">
        <div className="row">
          <Slider {...settings}>
            <div className="container">
              <div className="row">
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x1team.png" alt="" />
                    <div className="card-body">
                      <h6 className="text-center">Dinesh karki</h6>
                      <p>
                        Dinesh karki is co-founder and CEO at Codesmith – a
                        software engineering and machine learning residency
                        based in Los Angeles, New York, and Oxford
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x2team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x3team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x3team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x1team.png" alt="" />
                    <div className="card-body">
                      <h6 className="text-center">Dinesh karki</h6>
                      <p>
                        Dinesh karki is co-founder and CEO at Codesmith – a
                        software engineering and machine learning residency
                        based in Los Angeles, New York, and Oxford
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x2team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x3team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x3team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x1team.png" alt="" />
                    <div className="card-body">
                      <h6 className="text-center">Dinesh karki</h6>
                      <p>
                        Dinesh karki is co-founder and CEO at Codesmith – a
                        software engineering and machine learning residency
                        based in Los Angeles, New York, and Oxford
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x2team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x3team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <img src="/assets/x3team.png" alt="" />
                    <h6 className="text-center">Dinesh karki</h6>
                    <p>
                      Dinesh karki is co-founder and CEO at Codesmith – a
                      software engineering and machine learning residency based
                      in Los Angeles, New York, and Oxford
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Sliders;
