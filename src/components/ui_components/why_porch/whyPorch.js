import React from "react";
import "./whyPorch.css";

const WhyPorch = props => {
  return (
    <div className="why_porch container">
      <div className="row giu textwhy">
        <div className="col-sm-6 visible-xs">
          <div id="myCarousel" className="carousel slide" data-ride="carousel">
            {/* <!-- Indicators --> */}
            <ol className="carousel-indicators two">
              <li
                data-target="#myCarousel"
                data-slide-to="0"
                className="active"
              ></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            {/* <!-- Wrapper for slides --> */}
            <div className="carousel-inner">
              <div className="item active">
                <img
                  src="/assets/servizio1.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </div>

              <div className="item">
                <img
                  src="/assets/servizio2.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </div>

              <div className="item">
                <img
                  src="/assets/servizio3.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </div>
              <div className="item">
                <img
                  src="/assets/servizio4.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>

        <div className="col-sm-6 hidden-xs lpad">
          <div className="row">
            {/* <!--<div className="col-sm-4"></div>--> */}

            <div className="col-sm-6">
              <img
                src="/assets/servizio1.png"
                className="center-block p2img"
                alt=""
              />{" "}
              <br />
              <img
                src="/assets/servizio3.png"
                className="center-block p2img"
                alt=""
              />
            </div>

            <div className="col-sm-6 xspazio">
              <img
                src="/assets/servizio2.png"
                className="center-block p2img"
                alt=""
              />{" "}
              <br />
              <img
                src="/assets/servizio4.png"
                className="center-block p2img"
                alt=""
              />
            </div>
          </div>
        </div>

        <div className="col-sm-6 mto visible-xs">
          <h2 className="why_procho">Why Use ClassPorch??</h2>
          <hr className="r media-left" /> <br />
          <br />
          <p className="">
            Lorem ipsum dolor sit amet, et ius tibique posidonium quaerendum,
            eum ne suas oportere. At quod indoctum maiestatis mei, vim simul
            albucius quaestio cu, eum purto laoreet indoctum at. <br />
            <br />
            Ut suas luptatum torquatos vis, ne usu phaedrum eleifend quaerendum.
            Liber veritus mel no, harum consul inermis nec ei, est eirmod
            eruditi epicuri ei. Mutat errem dicunt ad eam.
          </p>
        </div>

        <div className="col-sm-6 mto hidden-xs xspazio rpad">
          <h2 id="primary-heading">Why Use ClassPorch??</h2>
          <hr className="r media-left" /> <br />
          <br />
          <p className="why_para">
            Lorem ipsum dolor sit amet, et ius tibique posidonium quaerendum,
            eum ne suas oportere. At quod indoctum maiestatis mei, vim simul
            albucius quaestio cu, eum purto laoreet indoctum at. <br />
            <br />
            Ut suas luptatum torquatos vis, ne usu phaedrum eleifend quaerendum.
            Liber veritus mel no, harum consul inermis nec ei, est eirmod
            eruditi epicuri ei. Mutat errem dicunt ad eam.
          </p>
        </div>
      </div>
      {/* <!--row3-->			 */}
    </div>
  );
};

export default WhyPorch;
