import React from 'react';
import './join_our_team.css';


const JoinOurTeam = props =>{
    return (
             <div className="container">

             <div className="row mit"> 
		
        <div className="col-sm-12 text text-center">
            OUR GREAT MINDS
            <hr className="h"/>
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing eli</h4> <br/>
            <a href="#" className="jbutton"><span className="jcon">JOIN OUR TEAM</span></a>
        </div>

    </div>


    <div className="row">
				
				<div className="col-sm-3"></div>
				
				<div className="col-sm-2">
					<div id="demo"></div>
			    	<p className="text-center white_space_nowrap">Tony Nguyen, Chancellor</p>
			    	<div className="row social">
			    		<div className="col-sm-12 text-center">
			    			<img src="/assets/fb-black.png" className="pright10" alt=""/> 
			    		
			    			<img src="/assets/tw-black.png" className="pright10" alt=""/> 
			    		
			    			<img src="/assets/ista-black.png" className="" alt=""/>
			    		</div>
			    	</div>			    				    				    	
			    </div>
				
				<div className="col-sm-2">
					<div id="demo2"></div>
			    	<p className="text-center white_space_nowrap">John Dawson, Vice Chancellor</p>
			    	<div className="row social">
			    		<div className="col-sm-12 text-center">
			    			<img src="/assets/fb-black.png" className="pright10" alt=""/> 
			    		
			    			<img src="/assets/tw-black.png" className="pright10" alt=""/> 
			    		
			    			<img src="/assets/ista-black.png" className="" alt=""/>
			    		</div>
			    	</div>
				</div>
				
				<div className="col-sm-2">
					<div id="demo3"></div>
			    	<p className="text-center white_space_nowrap">Richard Rexan, Bursar</p>
			    	<div className="row">
			    		<div className="col-sm-12 text-center">
			    			<img src="/assets/fb-black.png" className="pright10" alt=""/> 
			    		
			    			<img src="/assets/tw-black.png" className="pright10" alt=""/> 
			    		
			    			<img src="/assets/ista-black.png" className="" alt=""/>
			    		</div>
			    	</div>
				</div>
				
				<div className="col-sm-3"></div>
				
			</div>

             </div>
    );

}

export default JoinOurTeam;