import React from 'react';
import './map.css';
import { GoogleMap, withScriptjs, withGoogleMap } from 'react-google-maps';
const Maps = props => {
    function map() {
       let lat =parseFloat(31.5204);
       let lng = parseFloat(74.3587)
        return (
            <GoogleMap zoom={10} defaultCenter={{ lat: lat, lng: lng }} />
        );
    }

    const WrappedMAP = withScriptjs(withGoogleMap(map));

    return (
        <div className="container maps">    
            <div className="row">

                <div className="col-sm-12 text text-center">
                    <h4>Find Our Location</h4>
                    <p className="numeri tmap">Maps & Directions</p>
                    <h4>Find out how to find us from your current location</h4>
                </div>

            </div>
            {/* <!--row1bis--> */}


            <br /><br />


            {/* <!--<hr className="h">--> */}

            <div className="row">
                <div className="col-sm-12">
                    <WrappedMAP loadingElement={<div style={{ height: `100%` }} />}
                        containerElement={<div style={{ height: `400px` }} />}
                        mapElement={<div style={{ height: `100%` }}/>}
                        googleMapURL={"https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"} />
                </div>
            </div>
        </div>
    );  
}

export default Maps; 