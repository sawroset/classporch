import React from "react";
import "./contact_us_live.css";
import { useState } from "react";
import constants from "../../../config";
import axios from "axios";
const ContactUs_Live = props => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [website, setWebsite] = useState("");
  const [msg, setMsg] = useState("");
  const [send, setsend] = useState(false);

  const handleName = e => {
    setName(e.target.value);
  };
  const handleEmail = e => {
    setEmail(e.target.value);
  };
  const handleWebsite = e => {
    setWebsite(e.target.value);
  };
  const handleMsg = e => {
    setMsg(e.target.value);
  };
  const submitForm = async e => {
    e.preventDefault();

    await axios
      .post(constants.Base_URL + "v1/send_contact_us_mail", {
        name: name,
        email: email,
        website: website,
        message: msg
      })
      .then(res => {
        setsend(true);
        console.log(res);
      });
  };
  return (
    <div className="c_u_l container">
      <div className="row">
        <div className="col-sm-12 text-center">
          <p className="numeri tmap">Contact us live</p>
          <h3 className="mbottom30">Have a question?</h3>
        </div>
      </div>

      <br />
      <br />

      <div className="row">
        <div className="col-sm-12 text-center cform">
          <img
            src="/assets/email.png"
            className="img-responsive center-block email"
            alt=""
          />
          <h3 className="emailt">Send Us An email</h3>

          <form
            onSubmit={submitForm}
            name="#"
            method="post"
            className="form-signin form-group"
          >
            <input
              onChange={handleName}
              type="text"
              className="cinput input-fields"
              name="#"
              placeholder="Name*"
              required
            />
            <br />

            <input
              onChange={handleEmail}
              type="text"
              className="cinput input-fields"
              name="#"
              placeholder="Email*"
              required
            />
            <br />

            <input
              onChange={handleWebsite}
              type="text"
              className="cinput input-fields"
              name="#"
              placeholder="Website*"
              required
            />
            <br />

            <textarea
              onChange={handleMsg}
              className="cinput input-fields"
              name="#"
              cols="20"
              rows="5"
              placeholder="Your message*"
              required
            ></textarea>
            <br />
            {send ? (
              <div className="alert alert-success">
                <strong>Success!</strong> Email send successfully.
              </div>
            ) : null}
            <button className="sbutton img-rounded" id="sBtn">
              <span className="scon">SUBMIT</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ContactUs_Live;
