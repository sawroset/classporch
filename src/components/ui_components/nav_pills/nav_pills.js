import React from 'react';
import './nav_pills.css';
import { NAVPILLS } from '../../../actions/search_tutor';
import { useDispatch, useSelector } from 'react-redux';
const Nav_Pills = props => {
    const dispatch = useDispatch();
    const handle_Pill = pill => {
        console.log("aksjkaskjkasj")
        console.log(pill)
        dispatch(NAVPILLS(pill));
    }


    return (
        <div className="nav_pills">
            <span onClick={() => { handle_Pill('Math') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'Math' ? 'materie' : 'pad')}>Math</span>
            <span onClick={() => { handle_Pill('English') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'English' ? 'materie' : 'pad')}>English</span>
            <span onClick={() => { handle_Pill('ESL') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'ESL' ? 'materie' : 'pad')}>ESL</span>
            <span onClick={() => { handle_Pill('Chinese') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'Chinese' ? 'materie' : 'pad')}>Chinese</span>
            <span onClick={() => { handle_Pill('GMAT') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'GMAT' ? 'materie' : 'pad')}>GMAT</span>
            <span onClick={() => { handle_Pill('SAT') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'SAT' ? 'materie' : 'pad')}>SAT</span>
            <span onClick={() => { handle_Pill('Computer') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'Computer' ? 'materie' : 'pad')}>Computer</span>
            <span onClick={() => { handle_Pill('Science') }} className={useSelector(state => state.SearchTutorSearchBar_NavPills == 'Science' ? 'materie' : 'pad')}>Science</span>
        </div>
    );
}
export default Nav_Pills;
