import React from "react";
import Rating_Star from "./get_rating_star";
const Ratings = props => {
  let read_more = false;
  let read_more_msg = "show more reviews";
  const toggle = () => {
    read_more = !read_more;
    if (read_more == true) {
      document.getElementById("show_less_more").innerHTML =
        "show less reviews <i class='fa fa-arrow-up' aria-hidden='true'></i>";
    } else {
      document.getElementById("show_less_more").innerHTML =
        "show more reviews  <i class='fa fa-arrow-down' aria-hidden='true'></i>";
    }

    console.log(read_more_msg);
  };

  const Read_more = () => {
    return (
      <div>
        <br />
        <br />
        <div
          onClick={toggle}
          data-target=".1"
          data-toggle="collapse"
          className="col-md-12"
        >
          <p id="show_less_more" className="show_more_rating">
            show more reviews
            <i
              className={
                read_more === true ? "fa fa-arrow-up" : "fa fa-arrow-down"
              }
              aria-hidden="true"
            ></i>
          </p>
        </div>
      </div>
    );
  };
  return props.ratings.map((item, index) => {
    return (
      <div key={index}>
        {index == "3" && read_more == false ? <Read_more /> : null}
        <div className={index > 2 ? "rating_box collapse 1" : "rating_box"}>
          <img
            src={item.profile_pic}
            className="img-responsive img-circle pull-left media-left irev"
            alt=""
          />
          <Rating_Star item={item.ratting} />
          <p className="">
            {item.name} - {item.role}
          </p>
          <p className="tit2plus">{item.comments}</p>
          <p className="corplus">{item.date}</p>
        </div>
      </div>
    );
  });
};

export default Ratings;
