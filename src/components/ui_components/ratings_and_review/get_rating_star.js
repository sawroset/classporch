import React from 'react';

const Rating_Star = props => {
    
            const One_star=    ()=>{
                return (
                   <div >
                       <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                )
            };   

            const Two_star=    ()=>{
                return (
                   <div >
                       <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                )
            };   

            const Three_star=    ()=>{
                return (
                   <div >
                       <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                )
            };   
            
            const Four_star=    ()=>{
                return (
                   <div >
                       <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                )
            };   

            
            const Five_star=    ()=>{
                return (
                   <div >
                       <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                        <i className="fa fa-star xfa-star" aria-hidden="true"></i>
                    </div>
                )
            };   
    return (
           <div>
               {props.item == '1' ? <One_star/> : null}
               {props.item == '2' ? <Two_star/> : null}
               {props.item == '3' ? <Three_star/> : null}
               {props.item == '4' ? <Four_star/> : null}
               {props.item == '5' ? <Five_star/> : null}  
           </div>
    );
}

export default Rating_Star;