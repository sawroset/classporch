import React from 'react';
import './ratings_and_review.css';
import Ratings  from './ratings';

const Ratings_and_Reviews = props => {

   
    return (
        <div className="container row ratings">       

            <div className="col-sm-12">
                <h2>Ratings & Reviews</h2>
                <hr className="h" align="left" />
                <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star mright8" aria-hidden="true"></i> 34 Reviews <br /><br />
                   <Ratings ratings={props.ratings}/>
                 
            </div>
        </div>      
    );  
};
export default Ratings_and_Reviews;
