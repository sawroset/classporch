import React from 'react';
import './video_play.css';

const VideoPlay = props => {
	return (

		<div className={props.class_name == "home_class" ? "container" : "container container_1"}>
			<div className="row hidden-xs">
				{/* <!--4 desktop--> */}

				{/* <!--<div className="col-sm-3"></div>--> */}

				<div className={props.class_name == "home_class" ? "col-sm-12  parent" : "col-sm-12  parent_750"}>

					<iframe width="100%" height="100%" src="https://www.youtube.com/embed/tgbNymZ7vqY">
					</iframe>
				</div>

				{/* <!--<div className="col-sm-3"></div>--> */}

			</div> 

			<div className="row visible-xs">
				{/* <!--4 mobile--> */}

				<div className="col-sm-3"></div>

				<div className={props.class_name == "home_class" ? "col-sm-6  parent" : "col-sm-6  parent_750"}>
					<iframe width="100%" height="100%" src="https://www.youtube.com/embed/tgbNymZ7vqY">
					</iframe>  
				</div>

				<div className="col-sm-3"></div>

			</div>
		</div>
	);
};

export default VideoPlay;