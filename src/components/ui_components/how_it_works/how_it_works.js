import React from "react";
import "./how_it_works.css";

const How_it_Work = props => {
  return (
    <div className="container how_work">
      <div className="row">
        <div className="col-sm-12 text text-center" id="primary-text">
          How It Works
        </div>
      </div>
      {/* <!--row1bis--> */}

      <hr className="h" />

      <div className="row loghi">
        {/* <!--<div className="col-sm-3 col-md-3 col-lg-3 col-xs-3"></div>--> */}

        <div className="col-sm-4 col-md-4 col-lg-4 col-xs-12">
          <img
            src="/assets/find2.jpg"
            className="img-responsive center-block"
            alt=""
          />{" "}
          <br />
          <h3 className="text-center" id="how_title">
            Find
          </h3>
          <p className="text-center" id="how_it_des">
            Ne has facer apeirian recteque, ad quis constituam instructior nam.
          </p>
        </div>

        <div className="col-sm-4 col-md-4 col-lg-4 col-xs-12">
          <img
            src="/assets/chat2.jpg"
            className="img-responsive center-block"
            alt=""
          />{" "}
          <br />
          <h3 className="text-center" id="how_title">
            Chat
          </h3>
          <p className="text-center" id="how_it_des">
            Ne has facer apeirian recteque, ad quis constituam instructior nam.
          </p>
        </div>

        <div className="col-sm-4 col-md-4 col-lg-4 col-xs-12">
          <img
            src="/assets/learn2.jpg"
            className="img-responsive center-block"
            alt=""
          />{" "}
          <br />
          <h3 className="text-center" id="how_title">
            Learn
          </h3>
          <p className="text-center" id="how_it_des">
            Ne has facer apeirian recteque, ad quis constituam instructior nam.
          </p>
        </div>

        {/* <!--<div className="col-sm-3"></div>-->	 				 */}
      </div>
    </div>
  );
};
export default How_it_Work;
