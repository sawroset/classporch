import React from "react";
import { Link } from "react-router-dom";
import "./forTutor.css";
import TutorSliders from "./TutorSlider";
import { FaCcPaypal, FaChalkboardTeacher, FaHome } from "react-icons/all";

const ForTutor = () => {
  return (
    <React.Fragment>
      <div id="tutorMain">
        <div className="container" id="forTutor">
          <div className="row">
            <div className="box-content">
              <h1 className="text-center">
                Get the best uni job in town. Become an online tutor.
              </h1>
              <Link to="/signup">Apply Now</Link>
            </div>
          </div>
        </div>
      </div>
      <div className="whyChoseUs">
        <h1 className="text-center">Why online tutoring?</h1>
        <p>
          We help you get regular, flexible work by making it easy to find
          online tutoring jobs.
        </p>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="card">
                <FaCcPaypal />
                <h3>Well paid</h3>
                <p>
                  Take home up to £24/hour, with no time or money spent on
                  travel. Sweet.
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <FaHome />
                <h3>Flexible</h3>
                <p>
                  Choose your hours and tutor from anywhere. All you need is a
                  computer.
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <FaChalkboardTeacher />
                <h3>Rewarding</h3>
                <p>
                  Change the course of someone's life – for real. (And get
                  skills for your CV…)
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <TutorSliders />
    </React.Fragment>
  );
};

export default ForTutor;
