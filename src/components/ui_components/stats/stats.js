import React from 'react';
import './stats.css';

const Stats = props => {
    return (
        <div className="row funder giu">
            <div className="container">

                <div className="ver">
                    {/* <!--<div className="col-sm-2"></div>--> */}

                    <div className="col-sm-3 text-center">
                        <span className="numeri">9,49,973</span> <br />
                        <span className="nscritte">Happy students</span>
                    </div>

                    <div className="col-sm-3 text-center">
                        <span className="numeri">10,05,515+</span> <br />
                        <span className="nscritte">Hours of Live Learning</span>
                    </div>

                    <div className="col-sm-3 text-center">
                        <span className="numeri">500+</span> <br />
                        <span className="nscritte">Clients WorldWide</span>
                    </div>

                    <div className="col-sm-3 text-center">
                        <span className="numeri">4.8/5</span> <br />
                        <span className="nscritte">Average Teacher Rating</span>
                    </div>

                    {/* <!--<div className="col-sm-2"></div>--> */}
                </div>
            </div>
            {/* <!--row4--> */}

        </div>  
    );
}
export default Stats;