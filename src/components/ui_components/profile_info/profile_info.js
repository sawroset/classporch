import React, { useState } from "react";
import "./profile_info.css";

const Profile_info = props => {
  const handleEdit = () => {};

  const book_session = (
    <div className="col-sm-3 text-center">
      <p className="xb">BOOK A SESSION</p>
    </div>
  );
  const msg = (
    <div className="col-sm-3 text-center">
      <p className="xm">MESSAGE</p>
    </div>
  );
  return (
    <div>
      <div className="container">
        <div className="row mbottom20">
          <div className="col-sm-12 text-center">
            <h1 className="mtop0 mbottom20">Profile</h1>
            <img
              src={props.profilePic}
              className=" profile_pic img-responsive img-circle center-block thumbnails"
              alt=""
            />{" "}
            <br />
            <h4>{props.profile_info.name}</h4>
            <i className="fa fa-star" aria-hidden="true"></i>{" "}
            <i className="fa fa-star" aria-hidden="true"></i>{" "}
            <i className="fa fa-star" aria-hidden="true"></i>{" "}
            <i className="fa fa-star" aria-hidden="true"></i>{" "}
            <i className="fa fa-star" aria-hidden="true"></i> 5.0 (
            {props.profile_info.rating}) <br />
            <i className="fa fa-map-marker" aria-hidden="true"></i>{" "}
            {props.profile_info.location} <br />
            <h4 className="mtop mbottom5">
              <b>USD${props.profile_info.price}</b>
            </h4>
            per 15 min
          </div>
        </div>
        <div className="row">
          <div className="col-sm-3"></div>

          {window.location.pathname != "/profile" ? book_session : null}
          {window.location.pathname != "/profile" ? msg : null}

          <div className="col-sm-3"></div>
        </div>
      </div>
    </div>
  );
};

export default Profile_info;
