import React from "react";
import "./tutor_subject.css";

const TutorSubjects = props => {
  return (
    <div className="container find_subject">
      <div className="row">
        <div className="col-sm-12 text text-center" id="text-tutors">
          Find Online Tutors in Any Subject
        </div>
      </div>
      {/* <!--row1bis--> */}
      <div className="row">
        <hr className="h" />
        <div className="all_toturs_box col-md-12 mtop25">
          <div className="toturs_box yellow">
            <div className="toturs_box_img">
              <img src="/assets/ico_5.png" alt="img2" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">computer science</h6>
          </div>
          <div className="toturs_box gray">
            <div className="toturs_box_img">
              <img src="/assets/ico_6.png" img="img3" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">engineering</h6>
          </div>
          <div className="toturs_box yellow">
            <div className="toturs_box_img">
              <img src="/assets/ico_7.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">foreign language</h6>
          </div>
          <div className="toturs_box gray">
            <div className="toturs_box_img">
              <img src="/assets/ico_8.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">professional</h6>
          </div>
          <div className="toturs_box yellow">
            <div className="toturs_box_img">
              <img src="/assets/ico_9.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">humanities</h6>
          </div>
        </div>
        <div className="all_toturs_box col-md-12">
          <div className="toturs_box gray">
            <div className="toturs_box_img">
              <img src="/assets/ico_10.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">history</h6>
          </div>
          <div className="toturs_box yellow">
            <div className="toturs_box_img">
              <img src="/assets/ico_11.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">mathematics</h6>
          </div>
          <div className="toturs_box gray">
            <div className="toturs_box_img">
              <img src="/assets/ico_12.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">science</h6>
          </div>
          <div className="toturs_box yellow">
            <div className="toturs_box_img">
              <img src="/assets/ico_13.png" alt="img1" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">test prep</h6>
          </div>
          <div className="toturs_box gray">
            <div className="toturs_box_img">
              <img src="/assets/ico_14.png" className="mbottom20" />
            </div>
            <h6 className="uppercase font_bold">social sciences</h6>
          </div>
        </div>
      </div>
      <div className="row see">
        <div className="col-sm-12 text-center">
          <a href="#" className="rbutton2">
            <span className="con">see all subject</span>
          </a>
        </div>
      </div>
    </div>
  );
};

export default TutorSubjects;
