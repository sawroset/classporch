import React from 'react';
import './input_filter.css';

const Input_Filter = props =>{
    return (
	<div className="sort hidden-xs">
					{props.title}
					<hr align="left" className="h"/> 					
					<div className="input-group input-group-sm">						
						<input onChange={props.handleChange} type="text" name="search" id="search_box" className='search_box form-control textform' placeholder={props.place_holder}/><span className="input-group-addon">GO</span>
				 	</div>  
				</div>
    );
}
export default Input_Filter;    