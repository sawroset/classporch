import React from  'react';
import './dropdown_filter.css';

const DropDown_Filter = props =>{

    const options = props.options.map((option,index)=>{
        return(
        				<option key={index}>{option}</option>						
        );
    });
    return(
        	<div className="sort hidden-xs">
					{props.title}
					<hr align="left" className="h"/> 
					<div className="form-group">					  
					  <select className="form-control" id="sel1">
		                   {options}
					  </select>
					</div>					 
					<br/>
				</div>
    );
};

export default DropDown_Filter;