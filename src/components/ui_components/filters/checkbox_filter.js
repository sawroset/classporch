import React from 'react';
import './dropdown_filter.css';
const CheckBox_Filter = props => {


	
	const checkboxes = props.checkboxes.map((checkbox,index) => {
		
		return (
			<div key={index}>
				<input onChange={props.handleChange} type="checkbox" value={checkbox} />{checkbox}  <br />
			</div>      
		);
	});
           
   

	return (
		<div className="sort hidden-xs">
			{props.title}
			<hr align="left" className="h" />
			{checkboxes}
		</div>
	);
}
  
export default CheckBox_Filter; 