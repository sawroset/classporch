import React from 'react';
import './filters.css';
import CheckBox_Filter from './checkbox_filter';
import Input_Filter from './input_filter';
import Star_Filter from './star_filter';

import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';



const Filters = props => {

    const dispatch = useDispatch();
    const [, forceUpdate] = useState();

    const [location,setLocation] = useState("");

    const hourly_rate = ["under 10$", "under 20$", "under 30$", "under 40$"];
    
    const gender = ["Male","Female"]; 
       
    const handlehourlyRate = async (e) => {
        let rates = props.hourlyRate;
        if (e.target.checked == false) {
            rates = await rates.filter((item) => {
                return item != e.target.value;
            });
            await props.handleHourlyRate(rates);
        }
        else {
            rates.push(e.target.value);
            await props.handleHourlyRate(rates);
        }

        
    }

    const handleLocation = (e) =>{
         setLocation(e.target.value)              
    }

    const handleforceUpdate=()=>{
        
        forceUpdate();
    }

    const handleMinRate  = (e) =>{
        console.log(e.target.value)
        props.handleMinRate(e.target.value,handleforceUpdate);
    }
    
    const handleMaxRate  = (e) =>{
        console.log(e.target.value)
        props.handleMaxRate(e.target.value,handleforceUpdate);
    } 

    return (
        <div className="col-sm-3 sort_colum">
            
            {/* <br /><br />
       <CheckBox_Filter
                title="hourly rate"
                checkboxes={hourly_rate}
                handleChange={props.handleHourlyRate}
            />
 */}
            <br /><br />


            <Input_Filter
             title="location"
             handleChange={handleLocation}
            />
            <br/>
            <br/>
            <CheckBox_Filter 
                title="gender"  
                checkboxes={gender}
                handleChange={props.handleGender}
            />

            <br /><br />

            <div className="rates_filter sort hidden-xs">
					<div className="col-sm-6">
                    Min Hourly Rate
					<hr align="left" className="h"/> 					
					<div className="input-group input-group-sm">						
						<select onChange={handleMinRate}>
                        {props.maxRate>25?<option value="25">25</option>:null}
                            {props.maxRate>35?<option value="35">35</option>:null}
                            {props.maxRate>45?<option value="45">45</option>:null}
                            {props.maxRate>55?<option value="55">55</option>:null}
                            {props.maxRate>65?<option value="65">65</option>:null}
                            {props.maxRate>75?<option value="75">75</option>:null}
                            {props.maxRate>85?<option value="85">85</option>:null}
                            {props.maxRate>95?<option value="95">95</option>:null}
                            {props.maxRate>100?<option value="100">100</option>:null}
                        </select>
				 	</div> 
                    </div>
                    <div className="col-sm-6">
                    Max Hourly Rate
					<hr align="left" className="h"/> 					
					<div className="input-group input-group-sm">						
						<select onChange={handleMaxRate}> 
                            {props.minRate<25?<option value="25">25</option>:null}
                            {props.minRate<35?<option value="35">35</option>:null}
                            {props.minRate<45?<option value="45">45</option>:null}
                            {props.minRate<55?<option value="55">55</option>:null}
                            {props.minRate<65?<option value="65">65</option>:null}
                            {props.minRate<75?<option value="75">75</option>:null}
                            {props.minRate<85?<option value="85">85</option>:null}
                            {props.minRate<95?<option value="95">95</option>:null}
                            {props.minRate<100?<option value="100">100</option>:null}
                        </select>
				 	</div> 
                    </div>
                     
				</div>     

            <br/><br/>

       
    

            {/* <Input_Filter 
                title="School"
                place_holder="school name ..."
                handleChange={handleSchoolName} 
            />   */}

            <br /><br />

            {/* <Input_Filter  
                title="Language"
                place_holder="language ..."
                handleChange={handleLanguage}
            />  */}

            <br /><br />
  
            {/* <CheckBox_Filter
                title="Status"
                checkboxes={["Online"]}
                handleChange={handleStatus}
            />
        */}
            <br /><br />



            <br /><br />
            {/* <Star_Filter 
            handleChange={handleStarReviews}
            /> */}
        </div>
    );
}

export default Filters;