import React from 'react';
import './star_filter.css';

const Star_Filter = props => {
	return (
 
		<div className="sort hidden-xs">
			Rating
					<hr align="left" className="h" />
			<input type="checkbox" onChange={props.handleChange} value="5"/> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i><br />
			<input type="checkbox" onChange={props.handleChange} value="4"/> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i> <br />
			<input type="checkbox" onChange={props.handleChange} value="3"/> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i> <br />
			<input type="checkbox" onChange={props.handleChange} value="2"/> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i> <br />
			<input type="checkbox" onChange={props.handleChange} value="1"/> <i className="fa fa-star xfa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i><i className="fa fa-star" aria-hidden="true"></i> <br />
		</div>
	);
}

export default Star_Filter;    