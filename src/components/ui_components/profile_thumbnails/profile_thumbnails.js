import React, { useState } from "react";
import { YearPicker, MonthPicker, DayPicker } from "react-dropdown-date";
import "./profile_thumbnails.css";
import axios from "axios";
import constants from "../../../config";
import Autosuggest from "react-autosuggest";
const ProfileThumbnails = props => {
  
  
  const subjects = props.tags.map(item => {
    return <span className="scuola2">{item.name}</span>;
  });

  
  const education = props.education.map(item => {
    return (
      <div className="col-sm-12 margin">
        <img
          src="/assets/dis1.png"
          className="img-responsive pull-left media-left"
          alt=""
        />
        <p className="titplus">{item.university_name}</p>
        <p className="tit2plus">{item.degree}</p>
        <p className="corplus">
          {item.start_education},{item.finish_educaiton}
        </p>
      </div>
    );
  });

  
  const AboutDiv = (
    <div>
      <span className="titplus mbottom5">{props.about.intro}</span>
      <p className="casella_txt">{props.about.discription}</p>
    </div>
  );

  
  const EducationDiv = <div>{education}</div>;

 
  const SubjectsDiv = <div>{subjects}</div>;


  const VideoDiv = (
    <iframe className="profile_video" width="100%" height="100%" src={props.video_url}></iframe>
  );

  return (
    <div className="container thumbnails_component">
      <div className="row">
        <div className="col-sm-6">
          <div className="casella mbottom20">
            <h3>
              About
              
            </h3>
            <hr align="left" className="h" />
            {AboutDiv}
          </div>

          <div className="casella mbottom20">
            <h3>
              Education
              
            </h3>
            <hr align="left" className="h" />
            {EducationDiv}
            <br />
          </div>
        </div>

        <div className="col-sm-6 sub">
          <div className="casella mbottom20">
            <h3>
              Subjects
              
            </h3>
            <hr align="left" className="h" />
            {SubjectsDiv}
          </div>

          <div className="col-sm-6  parent mtop0">
            <div className="casella mbottom20">
              <h3>
                Video
                
              </h3>

              {VideoDiv}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProfileThumbnails;
