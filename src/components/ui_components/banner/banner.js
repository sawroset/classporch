import React from "react";
import "./banner.css";
import { Link } from "react-router-dom";
const Banner = props => {
  return (
    <div className="banner-color">
      <div className="Banner row container-fluid">
        <div className="col-sm-6 col-md-6 mab">
          {props.tag_line.length > 0 && (
            <h1 className="hc">{props.tag_line}</h1>
          )}
          {props.sub_tag_line.length > 0 && (
            <p className="text_white my-3">{props.sub_tag_line}</p>
          )}{" "}
          <br />
          {props.btn_txt.length > 0 && (
            <Link to={props.link} className="rbutton">
              <span className="con">{props.btn_txt}</span>
            </Link>
          )}
        </div>

        <div className="col-sm-6 col-md-6">
          <img src="/assets/computer.png" className="img-responsive" alt="" />
        </div>
      </div>
    </div>
  );
};

export default Banner;
