import React from "react";
import "./who_we_are.css";

const Who_We_Are = props => {
  return (
    <div className="who_we_are container">
      <div
        className={
          props.class_name == "invert"
            ? "invert row mbottom20"
            : "row mbottom20"
        }
      >
        <div className="col-sm-6">
          <img src={props.img_src} className="img-responsive" alt="" />
        </div>

        <div className="col-sm-6">
          <h2>{props.title}</h2>
          <p>{props.discription}</p>
          <br />
          <br />
          <p>{props.discription_sub}</p>
        </div>
      </div>
    </div>
  );
};

export default Who_We_Are;
