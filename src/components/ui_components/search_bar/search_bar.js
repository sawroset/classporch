import React from 'react';
import './search_bar.css';
import { useState } from 'react';
import { useDispatch } from 'react-redux';

import { SEARCHBAR_LOCATION, SEARCHBAR_NAME } from '../../../actions/search_tutor';

const SearchBar = props => {


        const submitForm = e => {
                e.preventDefault();
                props.handleSearch(); 
        };


        return (

                <div className="container search_component">
                        <div className="row  search_form">


                                <div className="cform text-center">
                                        <h1>Search Online Tutors</h1>
                                        <hr className="h" />

                                        <form onSubmit={submitForm} name="#" method="post" className="form-signin form-group" >

                                                <div className="row">
                                                        <div className="col-sm-1"></div>

                                                        <div className="col-sm-4">
                                                                <input  onChange={props.handleSubjectName} type="text" className="cinput cinputma" name="#" placeholder="Enter a subject or course"  /><br />
                                                        </div>

                                                        <div className="col-sm-4">
                                                                <input  onChange={props.handleTutorName} type="text" className="cinput" name="#" placeholder="Enter tutor name"  /><br />
                                                        </div>
 
                                                        <div className="col-sm-2 ">
                                                                <button className="rbutton4 cinputmas">FIND TUTORS</button>
                                                        </div>

                                                        <div className="col-sm-1"></div>
                                                </div>

                                        </form>
                                </div>


                                <div className="col-sm-2"></div>

                        </div>
                        {/* <!--row1bis--> */}

                </div>
        );
}

export default SearchBar;