import React from "react";
import "./suggested_faq.css";

const Suggested_Faq = props => {
  const questions = props.questions.map((item, index) => {
    return (
      <div className="col-sm-12" key="index">
        <div className="plus">
          <p
            data-target={"#" + index}
            data-toggle="collapse"
            id="faq-title-faq"
          >
            {item.question} <i className="fa fa-plus" aria-hidden="true"></i>
          </p>
          <p id={index} className="collapse faq-text">
            {item.answer}
          </p>
        </div>
      </div>
    );
  });

  return (
    <div className="container faq_questions">
      <div className="row">
        {/* <!--<div className="col-sm-3"></div>--> */}

        <div className="col-sm-12">
          <h3 id="faq-title">
            <strong>{props.title}</strong>
          </h3>
        </div>

        {/* <!--<div className="col-sm-3"></div>--> */}
      </div>

      <br />
      <br />

      <div className="row">
        {/* <!--			<div className="col-sm-3">
				
			</div>--> */}

        <div className="">{questions}</div>
      </div>
    </div>
  );
};

export default Suggested_Faq;
