import React from 'react';
import './career_banner.css';
const CareerBanner = props =>{
    return (
	<div className="container row xorange">
				<div className="xver">
					<div className="col-sm-12 text-center mdev">
					<h3 className="hc">We’re hiring! Come join our team</h3>
					<p>We’re a growing team of parents, teachers, tutors and recent pupils, and we’re going from strength to strength</p> <br/>
					<a href="#" className="careers">VIEW CAREERS</a>
					</div>	
				</div>   
				
			</div>
    );
 
}

export default CareerBanner;