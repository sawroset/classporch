import React from 'react';
import '../ratings_and_review/ratings_and_review.css';
import './general_availability.css'
const General_Availability = props => {
return (
	<div className="col-sm-12 tables">
				<h2>General Availability</h2>
				<hr align="left" className="h"/>
				
				<div className="table-responsive gene_tbl">
					<table className="table">
					  <caption></caption>
					  <thead>
						<tr>
						  <td></td>
						  <td>Mon</td> 
						  <td>Tue</td>
						  <td>Wed</td>
						  <td>Thu</td>
						  <td>Fri</td>     
						  <td>Sat</td>
						  <td>San</td>
						</tr>   
					  </thead>
					  <tbody>
						<tr>
						  <td scope="row">Pre 9am</td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						</tr>
						<tr>
						  <td scope="row">9am-12pm</td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						</tr>
						<tr>
						  <td scope="row">12pm-3pm</td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						</tr>
						<tr>
						  <td scope="row">3pm-6pm</td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						</tr>
						<tr>
						  <td scope="row">6pm-9pm</td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						</tr>
						<tr>
						  <td scope="row">After 9pm</td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						  <td><i className="fa fa-check" aria-hidden="true"></i></td>
						</tr>
					  </tbody>
					</table>
				  </div>
                  {/* <!-- /example --> */}
			</div>
);
};

export default General_Availability;