import React from "react";
import "./our_story.css";
const OurStory = props => {
  return (
    <div className="container our_story mt-5">
      <div className="row ">
        <div className="col-sm-12 t_2 text text-center about-title">
          {props.title}
        </div>
      </div>
      <p className="mt-5">
        Edugraff emerged as an online tutoring company that helps K12 students
        to overcome their pain areas in academics by providing the exact
        resources they need. Edugraff is an initiative by Jojo Mathew, an NITC
        graduate with more than 25 years in various fields including Technology
        Management and Stem tutoring. Driving force behind the venture is his
        passion to support young children and teenagers of all levels. In
        edugraff, we focus on providing one to one live sessions, thus adding
        value to the school curriculum by supplementing it. For those who aspire
        to excel, the adaptive process leads them to challenge themselves. The
        tutors are from reputed institutions, carefully chosen to be highly
        adaptive to deliver online live classes.
      </p>
    </div>
  );
};

export default OurStory;
