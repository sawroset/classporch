import React, { useEffect } from "react";
import "./contact_us.css";
import Banner from "../ui_components/banner/banner";
import ContactUs_Live from "../ui_components/contact_us_live/contact_us_live";
import ContactInfo from "../ui_components/contact_info/contact_info";
import {
  Navbar_clr_white,
  Navbar_clr_grey,
  Background_clr_white,
  Background_clr_grey
} from "../../actions/theme";
import { useDispatch } from "react-redux";
const ContactUS = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(Background_clr_white());
    dispatch(Navbar_clr_white());

    return () => {
      dispatch(Background_clr_grey());
      dispatch(Navbar_clr_grey());
    };
  }, []);

  return (
    <div className="">
      <ContactUs_Live />
    </div>
  );
};

export default ContactUS;
