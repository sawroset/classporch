import React, { useEffect } from "react";
import "./home.css";
import Banner from "../ui_components/banner/banner";
import How_it_Work from "../ui_components/how_it_works/how_it_works";
import VideoPlay from "../ui_components/video_play/video_play";
import WhyPorch from "../ui_components/why_porch/whyPorch";
import Stats from "../ui_components/stats/stats";
import Slider from "../ui_components/slider/slider";
import TutorSubjects from "../ui_components/tutor_subject/tutor_subject";
const Home = props => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <Banner
        tag_line="Searching for Tutor let's work Together"
        sub_tag_line="We are providong tutor where you can find instructor and student We are providong tutor where"
        btn_txt="Find Tutors"
        link="/searchtutor"
      />

      <How_it_Work />

      <WhyPorch />

      <Slider />

      <TutorSubjects />
    </div>
  );
};

export default Home;
