import { Background_clr_reducer, Navbar_show } from './theme';

import { HourlyRate, LEVEL, STATUS, RATING, SCHOOL_NAME, LANGUAGE, LOCATION, SEARCHBAR_NAME, SEARCHBAR_LOCATION, NAVPILLS } from './search_tutor';

import { authentication_status, authenticated_user_info,user_role } from './auth';
import { combineReducers } from 'redux';


const root_reducer = combineReducers({
    user_role:user_role,
    authentication_status: authentication_status,
    authenticated_user_info: authenticated_user_info,
    background_clr: Background_clr_reducer,
    Navbar: Navbar_show,
    SearchTutorHourlyRate: HourlyRate,
    SearchTutorLevel: LEVEL,
    SearchTutorOnlineStatus: STATUS,
    SearchTutorRating: RATING,
    SearchTutorLocation: LOCATION,
    SearchTutorSchoolName: SCHOOL_NAME,
    SearchTutorLanguage: LANGUAGE,
    SearchTutorSearchBar_Name: SEARCHBAR_NAME,
    SearchTutorSearchBar_Location: SEARCHBAR_LOCATION,
    SearchTutorSearchBar_NavPills: NAVPILLS
});

export default root_reducer;
