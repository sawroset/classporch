export const authentication_status = (state = false, action) => {
    switch (action.type) {
        case 'login':
            return state = true;
        case 'logout':
            localStorage.removeItem('auth_token');
            return state = false;
        default:
            return state;

    }
}

export const authenticated_user_info = (state = {}, action) => {
    switch (action.type) {
        case 'save_auth_user_info':
            return state = action.payload;
        case 'remove_auth_user_info':
            return state = {};
        default:
            return state;
    }
}

export const user_role = (state ="",action)=>{
     switch (action.type){
         case "save_user_role":
             return state = action.payload;
         case "remove_user_role":
             return state = "";
        default:
            return state;
     }
}
