export const HourlyRate = (state = [], action) => {

    switch (action.type) {
        case 'rates':
            return state = action.payload;
        default:
            return state;
    }
}

export const LEVEL = (state = [], action) => {

    switch (action.type) {
        case 'levels':
            return state = action.payload;
        default:
            return state;
    }
}

export const STATUS = (state = false, action) => {
    switch (action.type) {
        case 'toggle':
            return state = action.payload;
        default:
            return state;
    }
}

export const RATING = (state = [], action) => {
    switch (action.type) {
        case 'rating':
            return state = action.payload;
        default:
            return state;
    }
}


export const LOCATION = (state = '', action) => {
    switch (action.type) {
        case 'location':
            return state = action.payload;
        default:
            return state;
    }
}


export const SCHOOL_NAME = (state = '', action) => {
    switch (action.type) {
        case 'school_name':
            return state = action.payload;
        default:
            return state;
    }
}
export const LANGUAGE = (state = '', action) => {
    switch (action.type) {
        case 'language':
            return state = action.payload;
        default:
            return state;
    }
}

export const SEARCHBAR_NAME = (state = '', action) => {
    switch (action.type) {
        case 'searchbar_name':
            return state = action.payload;
        default:
            return state;
    }
}

export const SEARCHBAR_LOCATION = (state = '', action) => {
    switch (action.type) {
        case 'searchbar_location':
            return state = action.payload;
        default:
            return state; 
    }
}

export const NAVPILLS = (state = 'Math', action) => {
    switch (action.type) {
        case 'nav_pills_items':
            return state = action.payload;
        default:
            return state;
    }
}